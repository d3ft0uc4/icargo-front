import React from 'react'
import cn from 'classnames'
import { Field } from 'react-final-form'

const Input = ({
  label,
  hint,
  hideError,
  autoComplete = false,
  maxWidth,
  required,
  ...restProps
}) => (
  <Field {...restProps}>
    {({ input, meta, placeholder, ...restFieldProps }) => {
      const isInvalid = (meta.error || meta.submitError) && meta.submitFailed
      return (
        <div className="row mt-2">
          {label && (
            <div className="col-lg-2" style={{ height: '45px' }}>
              <p className={'form-input-label'}>
                {label}{' '}
                {!required ? <></> : <span style={{ color: 'red' }}>*</span>}
              </p>
            </div>
          )}
          <div className="col-lg-5">
            <input
              {...input}
              {...restFieldProps}
              placeholder={placeholder}
              className={cn({
                'form-input': true,
                'is-danger': isInvalid,
              })}
              autoComplete={autoComplete ? 'on' : 'off'}
              required={required}
            />
            {isInvalid && !hideError && (
              <p
                className={cn({
                  help: true,
                  'is-danger': isInvalid,
                })}
              >
                {meta.error || meta.submitError}
              </p>
            )}
            {!!hint && <p className={'help'}>{hint}</p>}
          </div>
        </div>
      )
    }}
  </Field>
)

export default Input
