import React from 'react'

const FormField = ({
  lclass,
  label,
  required,
  children,
  style,
  name,
  hide,
}) => {
  const cn = label ? 'col-lg-6' : 'col-lg-6 offset-lg-4 '
  return (
    <>
      {!hide && (
        <div className="row mt-2" style={style}>
          {label && (
            <div className="col-lg-4" style={{ height: '45px' }}>
              <p className={'form-input-label' + ' ' + lclass}>
                {label}{' '}
                {!required ? <></> : <span style={{ color: 'orange' }}>*</span>}
              </p>
            </div>
          )}
          {name ? (
            <div className={cn}>{children}</div>
          ) : (
            <div className={cn} name={name}>
              {children}
            </div>
          )}
        </div>
      )}
    </>
  )
}

export default FormField
