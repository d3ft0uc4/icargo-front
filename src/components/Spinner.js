import React from 'react'

const Spinner = ({ enabled }) => {
  return (
    <div style={{ display: enabled ? 'block' : 'none' }}>
      <div className="loading">
        <div className="spinner">
          <div className="rect1" />
        </div>
      </div>
    </div>
  )
}

export default Spinner
