import React from 'react'
import Header from './Header'
import Footer from './Footer'

const Layout = ({ children, ...rest }) => (
  <>
    <div className="wrapper">
      {' '}
      <Header {...rest} />
      <main className="container">{children}</main>
    </div>
    <Footer />
  </>
)

export default Layout
