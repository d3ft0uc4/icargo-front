import FilterBlock from './FilterBlock'
import HistoryBalanceTable from './HistoryBalanceTable'
import React, { useState, useEffect } from 'react'
import { sortOptionsTxs } from '../../utils/sorts'
import sortIcon from '../../assets/img/sortIcon.png'
import filterIcon from '../../assets/img/filterIcon.png'
import Select from 'react-select'
import sortSelectStyle from '../../utils/sortSelectStyle'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/fontawesome-free-solid'
import axios from 'axios'
import { GET_BALANCE_HISTORY } from '../../lib/backendRoutes'
import AnimatedDiv from '../AnimatedDiv'
const faStyle = {
  position: 'absolute',
  top: '15px',
  right: '6px',
}
const faClass = 'input-angle orange d-none d-md-block d-lg-none d-xl-block'
const filters = [
  {
    label: 'Вывод средств',
    value: 0,
  },
  {
    label: 'Подтвержден',
    value: 1,
  },
  { label: 'Начислено', value: 2 },
]
const HistoryBalance = ({ token }) => {
  const [mounted, setMounted] = useState(false)
  const [sortOptionTx, setSortOptionTx] = useState(sortOptionsTxs[0])
  const [chosenFilters, setChosenFilters] = useState(new Set())
  const [asc, setAsc] = useState(true)
  const [userTxs, setUserTxs] = useState([])
  const [defaultUserTxs, setDefaultUserTxs] = useState([])
  const sortHistory = (oldVal, newVal) => {
    if (newVal.value === 'default') {
      setUserTxs(Object.assign([], defaultUserTxs))
      setAsc(true)
    } else {
      let s
      s = userTxs.sort(newVal.comparator)
      if (asc && oldVal.value === newVal.value) {
        s = s.reverse()
      }
      setUserTxs(s)
      if (oldVal.value === newVal.value) {
        setAsc(!asc)
      } else {
        setAsc(true)
      }
    }
  }

  useEffect(() => {
    axios
      .post(
        GET_BALANCE_HISTORY,
        {
          filter: {
            operation: [...chosenFilters],
          },
          sort: sortOptionTx.value,
          asc: asc,
          skip: 0,
          limit: 0,
        },
        { headers: { token: token } }
      )
      .then(response => {
        const data = response.data
        setUserTxs(data)
        // setDefaultUserTxs(Object.assign([], data))
        setMounted(true)
      })
      .catch()
  }, [token, asc, sortOptionTx, chosenFilters])

  return (
    <AnimatedDiv pose={mounted ? 'visible' : 'hidden'}>
      <>
        <div
          className="mt-1 mb-1"
          style={{ paddingLeft: '0', paddingRight: '0', display: 'flex' }}
        >
          <h5
            className="col-5"
            style={{
              float: 'left',
              paddingTop: '11px',
              fontWeight: 100,
              paddingLeft: 0,
            }}
          >
            История операций
          </h5>
          <div className="col-3" style={{ paddingTop: '3px', paddingRight: 0 }}>
            <img
              src={filterIcon}
              alt=""
              width="15"
              height="15"
              style={{ position: 'absolute', top: '12px', left: '0px' }}
            />
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
              styles={sortSelectStyle}
              name="sort_type"
              required
              id="sort_type"
              defaultValue={{ label: 'Тип операции' }}
              onChange={e => {
                setChosenFilters(new Set([...chosenFilters.values(), e.label]))
              }}
              isDisabled={false}
              value={{ label: 'Тип операции' }}
              options={filters}
            />
            <FontAwesomeIcon
              icon={faAngleDown}
              className={faClass}
              style={faStyle}
              size="sm"
            />
          </div>
          <div className="offset-1"></div>
          <div
            className="col-3 col-md-3"
            style={{ paddingTop: '3px', paddingRight: 0 }}
          >
            <img
              src={sortIcon}
              alt=""
              width="15"
              height="15"
              style={{ position: 'absolute', top: '12px', left: '0' }}
            />
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
              styles={sortSelectStyle}
              name="sort_type"
              required
              id="sort_type"
              defaultValue={sortOptionsTxs[0]}
              value={sortOptionTx}
              onChange={e => {
                sortHistory(sortOptionTx.value, e.value)
                setSortOptionTx(e)
              }}
              isDisabled={false}
              options={sortOptionsTxs}
            />
            <FontAwesomeIcon
              icon={faAngleDown}
              className={faClass}
              style={faStyle}
              size="sm"
            />
          </div>
        </div>

        <div className="row">
          <div className="col-12">
            {' '}
            <FilterBlock elems={chosenFilters} handler={setChosenFilters} />
          </div>
        </div>
        <div className="col-lg-12">
          <div>
            <div className="row">
              <HistoryBalanceTable data={userTxs} />
            </div>
          </div>
        </div>
      </>
    </AnimatedDiv>
  )
}
export default HistoryBalance
