import React from 'react'
import { useHistory } from 'react-router'
import toFixed from '../../utils/toFixed'
const BalanceTable = ({ data, className }) => {
  const history = useHistory()
  const d = data ? data : []

  return (
    <div className={className} id="tariff">
      <div className="row">
        <div className="col-lg-12 text-center">
          <table className="table table-responsive w-100 d-block d-md-table text-white mt-2">
            <thead className="table-head table-head2">
              <tr className="">
                <th className="br-b">№</th>
                <th className="br-b">№ Заявки</th>
                <th className="br-b">Пункт вылета</th>
                <th className="br-b">Пункт назначения</th>
                <th className="br-b">Тип груза</th>
                <th className="br-b">Тариф, руб./кг</th>
                <th className="br-b">Вес / Платный вес, кг</th>
                <th className="br-b">Стоимость заказа, руб.</th>
                <th className="br-b">Кешбек</th>
                <th className="br-b">Статус заявки</th>
                <th style={{ padding: '5px' }}>Действие</th>
              </tr>
            </thead>
            <tbody>
              {d.map((row, i) => (
                <tr style={{ fontSize: '13px' }}>
                  <td>{i + 1}</td>
                  <td>{row.id_order}</td>
                  <td>
                    {row.airport_from_city} / {row.airport_from_iata}
                  </td>
                  <td>
                    {row.airport_to_city} / {row.airport_to_iata}
                  </td>
                  <td style={{ borderRight: 'none' }}>{row.name_type_cargo}</td>
                  <td>{row.rate}</td>
                  <td>
                    {toFixed(row.weight)} / {toFixed(row.weight_paid)}
                  </td>
                  <td>{toFixed(row.value)}</td>
                  <td>{toFixed(row.cachback)}</td>
                  <td>{row.description}</td>
                  <td>
                    {row.id_state < 11 && (
                      <a
                        href="#"
                        className="orange"
                        onClick={e => {
                          e.preventDefault()
                          history.push({
                            pathname: '/new',
                            state: {
                              id_order: row.id_order,
                            },
                          })
                        }}
                      >
                        Продолжить
                      </a>
                    )}
                    {row.id_state === 11 && (
                      <span className="orange">Ожидание оплаты</span>
                    )}
                    {row.id_state > 11 && <span className="orange"> - </span>}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default BalanceTable
