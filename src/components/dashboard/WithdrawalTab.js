import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import mc from '../../assets/img/mastercard.svg'
import visa from '../../assets/img/visa.svg'
import InputMask from 'react-input-mask'
import { HashLink as Link } from 'react-router-hash-link'
import Cookies from 'universal-cookie'
import axios from 'axios'
import { GET_BALANCE, CREATE_WITHDRAWAL } from '../../lib/backendRoutes'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/fontawesome-free-solid'
import Modal from 'react-modal'
import Spinner from '../Spinner'
import AnimatedDiv from '../AnimatedDiv'
import toFixed from "../../utils/toFixed";

const customModal = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    background: '#002666',
    border: 'none',
    maxWidth: '600px',
  },
  overlay: {
    background: 'rgba(0,0,0,0.2)',
    zIndex: 1050,
  },
}

const BalanceLine = styled.div`
  font-size: 18.5px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
`
const CardsLogos = styled.div`
  float: right;
  padding-top: 10px;
`

const Rub = styled.span`
  position: absolute;
  color: #757575;
  top: 19px;
  right: 30px;
`

const parseCash = v => {
  return v ? parseFloat(v).toFixed(2) : '0.00'
}
const WithdrawalTab = ({ token, idContact }) => {
  const [cardActive, setCardActive] = useState(false)
  const [errMsg, setErrMsg] = useState('')
  const [cash, setCash] = useState({})
  const [successWithdraw, setSuccessWithdraw] = useState(false)
  const [loading, setLoading] = useState(false)
  const [mounted, setMounted] = useState(false)
  useEffect(() => {
    axios
      .post(GET_BALANCE, {}, { headers: { token: token } })
      .then(response => {
        setCash(response.data)
        setMounted(true)
      })
      .catch()
  }, [idContact, token])

  return (
    <AnimatedDiv pose={mounted ? 'visible' : 'hidden'}>
      <>
        <div className="col-lg-12 mt-1 mb-1" style={{ paddingLeft: '0' }}>
          <h5
            style={{
              float: 'left',
              paddingTop: '11px',
              fontWeight: 100,
              minWidth: '100%',
            }}
            className="orange"
          >
            Текущий баланс
          </h5>
        </div>
        <div className="">
          <Spinner enabled={loading} />
          <BalanceLine className="row pt-3">
            <div className="col-7 ">Ожидает начисления:</div>
            <div className="col-5">{toFixed(parseCash(cash.wait_cash))} руб.</div>
          </BalanceLine>
          <BalanceLine className="row pt-3">
            <div className="col-7">Доступно:</div>
            <div className="col-5 orange">{toFixed(parseCash(cash.real_cash))} руб.</div>
          </BalanceLine>
          {!cardActive && (
            <BalanceLine className="row">
              <div className="col-7" />
              <div className="col-5 orange">
                <a
                  href="#"
                  className="orange hover"
                  style={{ fontSize: '12px', fontWeight: '300' }}
                  onClick={() => setCardActive(true)}
                >
                  Вывести средства
                </a>
              </div>
            </BalanceLine>
          )}
          {cardActive && (
            <div className="row mt-1 mb-1">
              <div className="col-12">
                <h5
                  style={{
                    float: 'left',
                    paddingTop: '11px',
                    fontWeight: 100,
                  }}
                  className="orange"
                >
                  Вывод средств
                </h5>
                <CardsLogos>
                  <img src={mc} width="28" height="22" alt="" />
                  <img src={visa} alt="" width="40" height="13" />
                </CardsLogos>
              </div>
              <form
                id="form"
                style={{ width: '100%' }}
                onSubmit={e => {
                  e.preventDefault()
                  setLoading(true)
                  const formData = new FormData(document.getElementById('form'))
                  let object = {}
                  formData.forEach((value, key) => {
                    object[key] = value
                  })
                  axios
                    .post(CREATE_WITHDRAWAL, object, {
                      headers: { token: token },
                    })
                    .then(() => {
                      setSuccessWithdraw(true)
                    })
                    .catch(() => {})
                    .finally(() => {
                      setLoading(false)
                    })
                }}
              >
                <div className="col-12">
                  <InputMask
                    placeholder="Номер карты, 16 цифр"
                    type="text"
                    mask="9999 9999 9999 9999"
                    maskChar={null}
                    className="form-input"
                    required
                    name="card"
                  />
                </div>
                <div className="col-12 pt-2">
                  <input
                    placeholder="Имя владельца карты (англ.)"
                    type="text"
                    className="form-input"
                    required
                    name="name"
                  />
                </div>
                <div className="col-12 pt-2">
                  <input
                    placeholder="Фамилия владельца карты (англ.)"
                    type="text"
                    className="form-input"
                    required
                    name="surname"
                  />
                </div>
                <div className="col-12 pt-2">
                  <input
                    placeholder="Введите сумму"
                    type="number"
                    step="0.01"
                    min="10"
                    className="form-input"
                    required
                    name="sum"
                  />
                  <Rub>руб.</Rub>
                </div>
                <div className="col-12 pt-1">
                  <div
                    className="checkbox-wrapper "
                    style={{
                      fontSize: '12px',
                    }}
                  >
                    <label className="control control-checkbox">
                      Нажимая кнопку "Вывести средства", вы соглашаетесь с{' '}
                      <Link
                        className="text-white"
                        to="/lic"
                        target="_blank"
                        style={{ textDecoration: 'underline' }}
                      >
                        Лицензионным соглашением
                      </Link>
                      {' '}и{' '}
                      <Link
                          className="text-white"
                          to="/lic"
                          target="_blank"
                          style={{ textDecoration: 'underline' }}
                      >
                        Соглашением об обработке персональных данных
                      </Link>
                      <input type="checkbox" />
                      <div
                        className="control_indicator"
                        style={{
                          top: '10px',
                        }}
                      />
                    </label>
                  </div>
                </div>
                <div className="col-12">
                  <button
                    type="submit"
                    className="btn calc-btn mt-0 mb-1"
                    style={{
                      width: '100%',
                    }}
                  >
                    Вывести средства
                  </button>
                </div>
                <div className="col-12">
                  <p className="orange">{errMsg}</p>
                </div>
              </form>
            </div>
          )}
          <Modal
            isOpen={successWithdraw}
            contentLabel="Example Modal"
            style={customModal}
          >
            <div
              style={{
                position: 'absolute',
                top: '12px',
                right: '20px',
                zIndex: 1060,
              }}
              onClick={() => document.location.reload(true)}
            >
              <FontAwesomeIcon
                icon={faTimes}
                style={{ width: '20px', height: '20px' }}
                size="lg"
              />
            </div>

            <div className="row">
              <div className="col-lg-12 text-center">
                <h2 className="mt-2 orange">Успешно</h2>
                <p className="text text-justify mb-0">
                  Заявка на вывод принята
                </p>
              </div>
            </div>
          </Modal>
        </div>
      </>
    </AnimatedDiv>
  )
}

export default WithdrawalTab
