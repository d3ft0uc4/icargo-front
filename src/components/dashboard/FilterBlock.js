import cross from '../../assets/img/cross.png'
import styled from 'styled-components'
import React from 'react'
const Wrapper = styled.div``

const FilterElem = styled.div`
  display: inline-block;
  width: 101px;
  height: 23px;
  border-radius: 2px;
  box-shadow: 0 0 7px 0 rgba(0, 0, 0, 0.7);
  background-color: #ff9700;
  margin-right: 10px;
`
const FilterSpan = styled.span`
  font-size: 10px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  padding-left: 10px;
  position: absolute;
  top: 8px;
`
const FilterButton = styled.img`
  height: 8px;
  width: 8px;
  position: absolute;
  top: 8px;
  right: 6px;
`

const FilterBlock = ({ elems, handler }) => {
  return (
    <>
      {!!elems.size && (
        <Wrapper>
          {' '}
          {Array.from(elems).map((elem, i) => {
            return (
              <>
                <FilterElem>
                  <div style={{ position: 'relative' }}>
                    <FilterSpan>{elem}</FilterSpan>
                    <FilterButton
                      src={cross}
                      onClick={() => {
                        elems.delete(elem)
                        handler(new Set(elems))
                      }}
                    />
                  </div>
                </FilterElem>
              </>
            )
          })}
          <a
            href="#"
            className="orange hover"
            style={{
              fontSize: '12px',
              fontWeight: '300',
              float: 'right',
              lineHeight: '23px',
              paddingRight: '10px',
            }}
            onClick={e => handler(new Set())}
          >
            Очистить все
          </a>
        </Wrapper>
      )}
    </>
  )
}
export default FilterBlock
