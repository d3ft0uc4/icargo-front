import React from 'react'
import { useHistory } from 'react-router'
import toFixed from '../../utils/toFixed'
const HistoryBalanceTable = ({ data, className }) => {
  const history = useHistory()
  const d = data ? data : []

  return (
    <div className={className} id="tariff" style={{ width: '100%' }}>
      <div className="row">
        <div className="col-lg-12 text-center">
          <table className="table table-responsive w-100 d-block d-md-table text-white mt-2">
            <thead className="table-head table-head2">
              <tr className="">
                <th className="br-b">№</th>
                <th className="br-b">Дата операции</th>
                <th className="br-b">Операция</th>
                <th className="br-b">Детали операции</th>
                <th className="">Сумма, руб.</th>
              </tr>
            </thead>
            <tbody>
              {d.map((row, i) => (
                <tr style={{ fontSize: '13px' }}>
                  <td>{i + 1}</td>
                  <td>{row.add_date}</td>
                  <td>{row.operation}</td>
                  <td>{row.description}</td>
                  <td style={{ borderRight: 'none' }}>{row.amount}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export default HistoryBalanceTable
