import Cookies from 'universal-cookie'
import React from 'react'

const DashboardNavbar = ({ active, setActive }) => {
  return (
    <div className="navbar navbar-expand-lg background-header dashboard-header-wrapper">
      <div className="container" style={{ padding: 0 }}>
        <p style={{ fontSize: '21px', marginBottom: 0 }}>Личный кабинет</p>
        <button
          className="navbar-toggle navbar-toggler  navbar-toggler-right"
          type="button"
          data-toggle="collapse"
          data-target="#navbarResponsive2"
          aria-controls="navbarResponsive2"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon icon-bar" />
        </button>
        <div className="collapse navbar-collapse" id="navbarResponsive2">
          <ul className="navbar-nav ml-auto dashboard-header">
            <li className="nav-item dashboard-item">
              <a
                href="#"
                className={'nav-link ' + (active === 'balance' ? 'orange' : '')}
                onClick={() => setActive('balance')}
              >
                Финансы
              </a>
            </li>
            <li className="nav-item dashboard-item">
              <a
                href="#"
                className={'nav-link ' + (active === 'history' ? 'orange' : '')}
                onClick={() => setActive('history')}
              >
                История
              </a>
            </li>
            <li className="nav-item dashboard-item">
              <a
                href="#"
                className={
                  'nav-link ' + (active === 'settings' ? 'orange' : '')
                }
                onClick={() => setActive('settings')}
              >
                Настройки
              </a>
            </li>
            <li className="nav-item">
              <a
                href="#"
                className="btn login-btn"
                role="button"
                onClick={() => {
                  const cookies = new Cookies()
                  cookies.set('token', '', {
                    path: '/',
                  })
                  cookies.set('id_contact', '', {
                    path: '/',
                  })
                  window.location.href = '/'
                }}
              >
                Выйти
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}
export default DashboardNavbar
