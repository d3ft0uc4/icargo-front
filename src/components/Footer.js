import React from 'react'
import { HashLink as Link } from 'react-router-hash-link'

const Footer = () => (
  <footer className="footer mt-auto">
    <div className="container text-center">
      <div className="mt-1 pr-1">
        <Link smooth className="pr-2 text-white" to="/#about">
          О сервисе
        </Link>
        <Link smooth className="pr-2 text-white" to="/#tariff">
          Тарифы
        </Link>
        <Link className="pr-2 text-white" to="/lic">
          Лицензионное соглашение
        </Link>
        <Link className="pr-2 text-white" to="/contact">
          Контакты
        </Link>
      </div>
      <div>
        <p>
          © 2019{' '}
          <a href="#" className="text-white">
            cargobooking.ru
          </a>
        </p>
      </div>
    </div>
  </footer>
)

export default Footer
