import React, { useState, useEffect } from 'react'

const Tab = ({
  id,
  children,
  label,
  hideToogler,
  initialOpen,
  labelRight,
  rightPanel,
  style,
  change,
}) => {
  const [opened, setOpened] = useState(!hideToogler && initialOpen)
  if (initialOpen) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
      setOpened(!hideToogler)
    }, [hideToogler])
  }

  return (
    <div className="row" style={style}>
      <div className="col-lg-12 mt-1 mb-1" id={id}>
        <h5 style={{ float: 'left', paddingTop: '11px', fontWeight: 100 }}>
          {label}
        </h5>
        {change && (
          <a
            href="#"
            onClick={e => {
              e.preventDefault()
              setOpened(!opened)
            }}
            className={'tab-toogle'}
          >
            <span className="pl-md-3" style={{ lineHeight: '48px' }}>
              {opened ? <>Скрыть </> : <>Показать </>}
            </span>
          </a>
        )}
        <span style={{ float: 'right', paddingTop: '13px' }} className="orange">
          {labelRight}
        </span>
        {rightPanel}
      </div>

      <div className="col-lg-12 ">
        <div className={opened ? 'opened' : 'closed'}>{children}</div>
      </div>
    </div>
  )
}

export default Tab
