import React, { useState, useEffect } from 'react'

const StepTab = ({
  id,
  children,
  label,
  labelRight,
  rightPanel,
  style,
  current,
  forceShow,
  progress,
  setProgress,
  initialProgress,
  setInitialProgress,
}) => {
  const [opened, setOpened] = useState(false)
  const [cont, setCont] = useState(false)
  useEffect(() => {
    setOpened(current <= initialProgress && progress !== current)
    setCont(current === initialProgress)
  }, [current, initialProgress, progress])
  return (
    <div className="row" style={style}>
      <div className="col-lg-12 mt-1 mb-1" id={id}>
        <h5 style={{ float: 'left', paddingTop: '11px', fontWeight: 100 }}>
          {label}
        </h5>
        <a
          href="#"
          onClick={e => {
            e.preventDefault()
            setProgress(current)
          }}
          className={'tab-toogle'}
        >
          {opened && (
            <span className="pl-md-3" style={{ lineHeight: '48px' }}>
              {cont ? 'Продолжить' : 'Изменить'}
            </span>
          )}
        </a>

        <span style={{ float: 'right', paddingTop: '13px' }} className="orange">
          {labelRight}
        </span>
        {rightPanel}
      </div>

      <div className="col-lg-12 ">
        <div>{children}</div>
      </div>
    </div>
  )
}

export default StepTab
