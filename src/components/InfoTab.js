import React, { useState, useEffect } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown, faAngleRight } from '@fortawesome/fontawesome-free-solid'

const InfoTab = ({
  children,
  label,
  hideToogler,
  initialOpen,
  labelRight,
  style,
}) => {
  const [opened, setOpened] = useState(!hideToogler && initialOpen)
  if (initialOpen) {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
      setOpened(!hideToogler)
    }, [hideToogler])
  }

  return (
    <div className="row" style={style}>
      <div className="col-lg-12 mt-1 mb-1">
        <h5 style={{ float: 'left', paddingTop: '11px', fontWeight: 100 }}>
          {label}
        </h5>
        {!hideToogler && (
          <a
            href="#"
            onClick={e => {
              e.preventDefault()
              setOpened(!opened)
            }}
            className={'tab-toogle'}
            style={{ height: '20px', paddingTop: '11px' }}
          >
            <span className="pl-3">
              {opened ? (
                <>
                  Скрыть{' '}
                  <FontAwesomeIcon
                    icon={faAngleDown}
                    style={{ paddingLeft: '5px', paddingTop: '11px' }}
                    size="2x"
                  />
                </>
              ) : (
                <>
                  Показать{' '}
                  <FontAwesomeIcon
                    icon={faAngleRight}
                    style={{ paddingLeft: '5px', paddingTop: '13px' }}
                    size="2x"
                  />
                </>
              )}
            </span>
          </a>
        )}
        <span style={{ float: 'right', paddingTop: '13px' }} className="orange">
          {labelRight}
        </span>
      </div>

      <div className="col-lg-12 ">
        <div className={opened ? 'opened' : 'closed'}>{children}</div>
      </div>
    </div>
  )
}

export default InfoTab
