import React from 'react'

const Checkbox = ({ name }) => {
  return (
    <>
      <div className="checkbox-wrapper">
        <label className="control control-checkbox">
          First checkbox
          <input type="checkbox" />
          <div className="control_indicator" />
        </label>
      </div>
    </>
  )
};

export default Checkbox
