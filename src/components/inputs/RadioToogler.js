import React from 'react'

const RadioToogler = ({ name, values, inverted, selectedValue }) => {
  return (
    <>
      <div className="radio-button-group">
        {values.map((x, i) => (
          <label>
            <input
              type="radio"
              value={x.value}
              key={i}
              name={name}
              checked={x.value === selectedValue}
            />
            <span>{x.label}</span>
          </label>
        ))}
      </div>
    </>
  )
};

export default RadioToogler
