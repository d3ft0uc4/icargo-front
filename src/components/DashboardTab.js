import React from 'react'

const DashboardTab = ({ id, children, active }) => {
  if (active) {
    return (
      <>
        <div id={id}>{children}</div>
      </>
    )
  }
  return null
}

export default DashboardTab
