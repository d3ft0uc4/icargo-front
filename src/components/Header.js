import React, { useState, useEffect } from 'react'
import logo from 'assets/img/logo.png'
import { HashLink as Link } from 'react-router-hash-link'
import axios from 'axios'
import { CHECK_PHONE, GET_BALANCE, LOGIN } from '../lib/backendRoutes'
import InputMask from 'react-input-mask'
import Modal from 'react-modal'
import Cookies from 'universal-cookie'
import useGlobal from '../lib/hookstore'
import AnimatedDiv from './AnimatedDiv'
import toFixed from '../utils/toFixed'

const customModal = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    background: '#002666',
    border: 'none',
    maxWidth: '400px',
    padding: 0,
  },
  overlay: {
    background: 'rgba(0,0,0,0.2)',
    zIndex: 1050,
  },
}

const Header = ({}) => {
  const [globalState, globalActions] = useGlobal()
  const [needBalance, setNeedBalance] = useState(false)
  const [balance1, setBalance1] = useState('')
  const [balance2, setBalance2] = useState('')
  const cookies = new Cookies()
  const idContact = cookies.get('id_contact')
  const token = cookies.get('token')
  if (!globalState.phone) {
    if (idContact) {
      globalActions.setPhone(idContact)
    }
  }
  const [inputPhone, setInputPhone] = useState('')
  const [inputSMS, setInputSMS] = useState('')

  const [loginForm, setLoginForm] = useState(false)
  const [smsSent, setSmsSent] = useState(false)
  const [licAgreed, setLicAgreed] = useState(false)

  const [errMsg, setErrMsg] = useState('')
  useEffect(() => {
    axios
      .post(GET_BALANCE, {}, { headers: { token: token } })
      .then(response => {
        setNeedBalance(true)
        setBalance2(
          toFixed(
            response.data.wait_cash
              ? parseFloat(response.data.wait_cash).toFixed(2)
              : '0.00'
          )
        )
        setBalance1(
          toFixed(
            response.data.real_cash
              ? parseFloat(response.data.real_cash).toFixed(2)
              : '0.00'
          )
        )
      })
      .catch()
  }, [idContact, token])
  return (
    <>
      <nav
        className="navbar navbar-expand-lg fixed-top background-header"
        id="navbar"
      >
        <div className="container ">
          <a className="navbar-brand" href="/">
            <img src={logo} alt="" />
          </a>
          <button
            className="navbar-toggle navbar-toggler  navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive"
            aria-controls="navbarResponsive"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon icon-bar" />
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link smooth className="nav-link" to="/#about">
                  О сервисе
                </Link>
              </li>
              <li className="nav-item">
                <Link smooth className="nav-link" to="/#tariff">
                  Тарифы
                </Link>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  href="http://www.cargobooking.ru"
                  target="_blank"
                >
                  Сравнить тарифы
                </a>
              </li>
            </ul>
            <ul className="navbar-nav ml-auto">
              <li className="nav-item">
                <div className="navbar-phone-container">
                  <span className="navbar-phone">8 800 600 51 28</span>
                  <br />
                  <span className="navbar-phone-description">
                    Бесплатно по России
                  </span>
                </div>
              </li>
              {needBalance && (
                <AnimatedDiv pose={needBalance ? 'visible' : 'hidden'}>
                  <li className="nav-item">
                    <div className="navbar-phone-container">
                      <span
                        className="navbar-phone orange"
                        style={{ float: 'right' }}
                      >
                        {balance1} руб.
                      </span>
                      <br />
                      <span style={{ float: 'right' }}>{balance2} руб.</span>
                    </div>
                  </li>
                </AnimatedDiv>
              )}

              {!globalState.phone && (
                <li className="nav-item">
                  <button
                    href="#"
                    onClick={() => setLoginForm(true)}
                    className="btn login-btn"
                    role="button"
                  >
                    Войти
                  </button>
                </li>
              )}
              {globalState.phone && (
                <li className="nav-item">
                  <Link to="/dashboard" className="btn login-btn phone-btn">
                    {globalState.phone}
                  </Link>
                </li>
              )}
            </ul>
          </div>
        </div>
      </nav>
      <Modal
        isOpen={loginForm}
        style={customModal}
        onRequestClose={() => {
          setLoginForm(false)
          setLicAgreed(false)
          setSmsSent(false)
        }}
      >
        <div className="container">
          {' '}
          <div className="login-form">
            <div className="login-form-inner">
              <form
                onSubmit={e => {
                  e.preventDefault()
                  if (!smsSent) {
                    axios
                      .post(CHECK_PHONE, {
                        phone_number: parseInt(
                          inputPhone.replace(/\D/g, '').slice(0, 11)
                        ),
                      })
                      .then(d => {
                        if (d.data.status_code === 100) {
                          setSmsSent(true)
                        }
                      })
                  } else {
                    axios
                      .post(LOGIN, {
                        id_contact: parseInt(
                          inputPhone.replace(/\D/g, '').slice(0, 11)
                        ),
                        code: parseInt(inputSMS),
                      })
                      .then(response => {
                        const cookies = new Cookies()
                        cookies.set('token', response.data.token, {
                          path: '/',
                        })
                        cookies.set(
                          'id_contact',
                          parseInt(inputPhone.replace(/\D/g, '').slice(0, 11)),
                          {
                            path: '/',
                          }
                        )
                        globalActions.setPhone(
                          parseInt(inputPhone.replace(/\D/g, '').slice(0, 11))
                        )
                        setLoginForm(false)
                        window.location.reload()
                      })
                      .catch(d => {
                        setErrMsg('Неправильный код СМС')
                      })
                  }
                }}
              >
                <h5 className="login-header">
                  {' '}
                  {smsSent
                    ? 'Введите код из полученного СМС:'
                    : 'Введите номер телефона: '}
                </h5>
                <InputMask
                  className="form-input"
                  placeholder="+7 (999) 999-99-99"
                  mask="+7 (999) 999-99-99"
                  maskChar={null}
                  id="input-phone"
                  alwaysShowMask={true}
                  value={inputPhone}
                  onInput={e => setInputPhone(e.target.value)}
                  disabled={smsSent}
                />
                {!smsSent && (
                  <>
                    {' '}
                    <div
                      className="checkbox-wrapper "
                      style={{ fontSize: '12px' }}
                    >
                      <label
                        className="control control-checkbox"
                        style={{ paddingTop: '20px' }}
                      >
                        Входя в аккаунт, вы соглашаетесь с{' '}
                        <Link
                          className="text-white"
                          to="/lic"
                          target="_blank"
                          style={{ textDecoration: 'underline' }}
                        >
                          Лицензионным соглашением и Соглашением об обработке
                          персональных данных
                        </Link>
                        <input
                          type="checkbox"
                          value={licAgreed}
                          onChange={e => setLicAgreed(!licAgreed)}
                        />
                        <div
                          className="control_indicator"
                          style={{ left: '25px', top: '125px' }}
                        />
                      </label>
                    </div>
                    <button
                      type="submit"
                      className="btn calc-btn mt-0 mb-1"
                      style={{
                        width: '100%',
                        display:
                          inputPhone.replace(/\D/g, '').slice(0, 11).length ===
                            11 &&
                          !smsSent &&
                          licAgreed
                            ? 'block'
                            : 'none',
                      }}
                    >
                      Войти
                    </button>
                  </>
                )}
                {smsSent && (
                  <>
                    <>
                      <InputMask
                        className="form-input"
                        placeholder="0123"
                        mask="9999"
                        maskChar={null}
                        id="input-sms"
                        style={{ marginTop: '15px' }}
                        alwaysShowMask={true}
                        value={inputSMS}
                        onInput={e => setInputSMS(e.target.value)}
                      />
                      <div
                        className="checkbox-wrapper "
                        style={{
                          fontSize: '12px',
                          height: '0',
                          paddingTop: '15px',
                          visibility: 'hidden',
                        }}
                      >
                        <label
                          className="control control-checkbox"
                          style={{ paddingTop: '20px' }}
                        >
                          Входя в аккаунт, вы соглашаетесь с{' '}
                          <Link
                            className="text-white"
                            to="/lic"
                            target="_blank"
                            style={{ textDecoration: 'underline' }}
                          >
                            Лицензионным соглашением и Соглашением об обработке
                            персональных данных
                          </Link>
                          <input type="checkbox" />
                          <div
                            className="control_indicator"
                            style={{
                              left: '25px',
                              top: '125px',
                              display: 'none',
                            }}
                          />
                        </label>
                      </div>
                      <button
                        type="submit"
                        className="btn calc-btn mt-0 mb-1"
                        style={{
                          width: '100%',
                        }}
                      >
                        Подтвердить
                      </button>
                    </>
                  </>
                )}
                {errMsg && (
                  <span
                    className="orange login-tip"
                    style={{ fontSize: '12px' }}
                  >
                    {errMsg}
                  </span>
                )}
              </form>
            </div>{' '}
          </div>
        </div>
      </Modal>
    </>
  )
}

export default Header
