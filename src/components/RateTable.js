import React from 'react'
import InfoTab from './InfoTab'
import { PoseGroup } from "react-pose";
const RateTable = ({ data, className }) => {
  const d = data ? data : []
  return (
    <div className={className} id="tariff">
      <div className="row">
        <div className="col-lg-12 text-center">
          <h2 className="mt-3">Тарифы на перевозку</h2>
          <table className="table table-responsive w-100 d-block d-md-table text-white mt-2">
            <thead className="table-head">
              <tr className="">
                <th rowSpan="2" className="br-b">
                  №
                </th>
                <th rowSpan="2" className="br-b" key="2">
                  Пункт вылета
                </th>
                <th rowSpan="2" className="br-b" key="3">
                  Пункт назначения
                </th>
                <th rowSpan="2" className="br-b" key="4">
                  Перевозчик
                </th>
                <th rowSpan="2" className="br-o" key="5">
                  Мин вес, кг.
                </th>
                <th colSpan="6" style={{ padding: '5px' }}>
                  Тариф за 1 кг, руб.
                </th>
              </tr>
              <tr className="">
                <th className="br-b" key="6">до 50 кг</th>
                <th className="br-b" key="7">до 100 кг</th>
                <th className="br-b" key="8">до 300 кг</th>
                <th className="br-b" key="9">до 500 кг</th>
                <th className="br-b" key="10">до 1000 кг</th>
                <th>свыше 1000 кг</th>
              </tr>
            </thead>
            <tbody>
              {d.map(row => (
                <tr>
                  {row.map(col => (
                    <td>{col}</td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      <InfoTab label="Правила применения тарифов, доплат и сборов">
        <div>
          {' '}
          <p className="text text-justify">
            Применение:
          </p>
          <ul className="dashed">
            <li>
              Тарифы, доплаты и сборы действуют на дату оформления грузовой авианакладной;
            </li>
            <li>
              Тарифы действуют "от аэропорта до аэропорта" независимо от наличия на маршруте перевозки груза промежуточных пунктов трансфера;
            </li>
            <li>
              Тарифы, доплаты и сборы применяются к платному весу груза, включая отправки, оформленные по минимальному сбору;
            </li>
          </ul>
          <p className="text text-justify">
            Платный вес:
          </p>
          <ul className="dashed">
            <li>
              Платный вес груз определяется наибольшим из значений между физическим и объемным весом груза;
            </li>
            <li>
              Объемный вес груза определяется путем деления его объема, выраженного в кубических метрах, на коэффициент 0,006 (Объемный вес, кг = Объем, м3 / 0,006);
            </li>
            <li>
              Округление платного веса производится до большего полукилограмма. Пример: 0,20 кг = 0,50 кг; 0,51 = 1,00 кг;
            </li>
          </ul>
          <p className="text text-justify">
            Минимальный сбор:
          </p>
          <ul className="dashed">
            <li>
              Минимальный сбор, подлежащий оплате, определяется путем умножения тарифа соответствующего грузового продукта на минимальный вес груза (действует для всех категорий груза);
            </li>
          </ul>
        </div>
      </InfoTab>
      <InfoTab label="Дополнительные сборы и доплаты">
        <div>
          {' '}
          <p className="text text-justify">
            Дополнительные сборы:
          </p>
          <ul className="dashed">
            <li>
              Топливный сбор (MYC) - для рейсов ПАО "Аэрофлот" отсутствует (регулируется в соответствии с топливной политикой ПАО "Аэрофлот");
            </li>
            <li>
              Тяжеловесный груз (более 80 кг / место) - + 5 руб / кг;
            </li>
          </ul>
          <p className="text text-justify">
            Оформление авианакладной (AWB):
          </p>
          <ul className="dashed">
            <li>
              рейсы ПАО "Аэрофлот" - 300 руб / AWB;
            </li>
          </ul>
        </div>
      </InfoTab>
      <InfoTab label="Прочие положения">
        <div>
          {' '}
          <p className="text text-justify">
            В тарифы и сборы не включается стоимость следующих услуг:
          </p>
          <ul className="dashed">
            <li>
              наземная доставка грузов в/из аэропортов;
            </li>
            <li>
              хранение сверх срока бесплатного хранения (если такой срок установлен), услуги грузового терминала или склада;
            </li>
            <li>
              расходы на страхование;
            </li>
            <li>
              расходы по таможенному оформлению груза, или аналогичные расходы, возникшие у третьих лиц (в т.ч. отправителя, получателя, владельца груза);
            </li>
            <li>
              сборы или штрафы, взимаемые органами государственной власти в отношении перевозимого груза, включая пошлины и налоги;
            </li>
            <li>
              расходы по устранению повреждений упаковки груза;
            </li>
            <li>
              сборы за перевозку груза другими видами транспорта, за переадресовку груза в другой пункт назначения или возврат в пункт отправления;
            </li>
          </ul>
        </div>
      </InfoTab>
      </div>
  )
}

export default RateTable
