import React from 'react'

const Progressbar = ({ order, date, progress }) => {
  const fmtDate = date => {
    return date ? new Date(date).toLocaleDateString('ru-RU') : ''
  }
  return (
    <div className="jumbotron-fluid background-header">
      <div className="container progress-bar-wrapper" id="progress-bar-wrapper">
        <div id="progressbar" className="row pb-3 background-header">
          <div
            className="col-lg-6"
            style={{ height: '40px', lineHeight: '40px' }}
          >
            <h5
              className="text justify"
              style={{
                float: 'left',
                height: '40px',
                width: '100%',
                lineHeight: '40px',
                textAlign: 'justify',
              }}
            >
              Заявка № <span className={'orange'}>{order}</span> от{' '}
              <span className={'orange'}>{date}</span> г. завершена на:{' '}
            </h5>
          </div>
          <div className="col-lg-6">
            <div
              className="progress "
              style={{ height: '40px', lineHeight: '40px' }}
            >
              <div
                className="progress-bar"
                role="progressbar"
                style={{ width: progress.toString() + '%' }}
                aria-valuenow={progress}
                aria-valuemin="0"
                aria-valuemax="100"
              />
              <p
                className="justify-content-center d-flex position-absolute w-100"
                style={{ color: '#042558', fontSize: '16px' }}
              >
                {progress}% из 100%
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Progressbar
