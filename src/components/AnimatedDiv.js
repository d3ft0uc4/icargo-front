import posed from 'react-pose'

const AnimatedDiv = posed.div({
  hidden: { opacity: 0 },
  visible: { opacity: 1 },
})

export default AnimatedDiv
