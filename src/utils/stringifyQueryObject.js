import querystring from 'querystring'

const stringifyQueryObject = (q = {}) => {
  const result = querystring.stringify(q)
  return result ? '?' + result : ''
}

export default stringifyQueryObject
