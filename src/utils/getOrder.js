import axios from 'axios'
import { GET_ORDER } from '../lib/backendRoutes'

const getOrder = (token, id) => {
  return axios
    .post(GET_ORDER, { id_order: parseInt(id) }, { headers: { token: token } })
    .then(response => {
      return response.data
    })
}

export default getOrder
