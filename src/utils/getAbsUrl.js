const getAbsUrl = (path) => {
  return `${process.env.REACT_APP_FRONTEND_URL}${!path || path === '/' ? '' : path}`
}

export default getAbsUrl
