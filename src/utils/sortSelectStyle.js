const sortSelectStyle = {
  menu: () => ({
    background: '#002a65',
    paddingTop: '0',
    position: 'absolute',
    width: '100%',
    zIndex: 9999,
    textAlign: 'initial',
  }),
  option: (styles, { data, isDisabled, isFocused, isSelected }) => ({
    ...styles,
    color: 'white',
    fontSize: '14px',
    width: '100%',
    background: '#002a65',
    '&:hover': {
      background: '#001b37',
    },
    '&:active': {
      background: '#001b37',
    },
  }),
  placeholder: () => ({ color: '#FF9600' }),

  control: (provided, state) => ({
    paddingTop: '0px',
    height: '35px',
    lineHeight: '40px',
    border: 'none',
    fontSize: '14px',
    fontWeight: '100',
    // width: '130px',
    background: state.menuIsOpen ? '#002a65' : 'transparent',
    textAlign: 'initial',
  }),
  singleValue: (provided, state) => {
    return {
      ...provided,
      height: '45px',
      borderRadius: 0,
      marginLeft: '4px',
      color: '#FF9600',
    }
  },
}

export default sortSelectStyle
