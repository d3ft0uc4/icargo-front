const sortOptions = [
  { label: 'по умолчанию', value: 'default' },
  {
    label: 'по номеру',
    value: 'number',
    comparator: (a, b) => (a.id_order <= b.id_order ? -1 : 1),
  },
  {
    label: 'по статусу',
    value: 'status',
    comparator: (a, b) => (a.id_state <= b.id_state ? -1 : 1),
  },
  {
    label: 'по кешбеку',
    value: 'cashback',
    comparator: (a, b) =>
      parseFloat(a.cachback || 0.0) <= parseFloat(b.cachback || 0.0) ? -1 : 1,
  },
]

const sortOptionsTxs = [
  { label: 'по умолчанию', value: 'default' },
  {
    label: 'по операции',
    value: 'operation',
    comparator: (a, b) => (a.operation <= b.operation ? -1 : 1),
  },
  {
    label: 'по дате',
    value: 'date',
    comparator: (a, b) => (a.add_date <= b.add_date ? -1 : 1),
  },
  {
    label: 'по сумме',
    value: 'sum',
    comparator: (a, b) => (a.id_order <= b.id_order ? -1 : 1),
  },
]
export { sortOptions, sortOptionsTxs }
