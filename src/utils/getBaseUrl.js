const getBaseUrl = () => {
  return process.env.REACT_APP_FRONTEND_URL || 'http://localhost:3000'
}

export default getBaseUrl
