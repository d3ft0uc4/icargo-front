import moment from 'moment'

const toHumanDate = (date, format) =>
  format ? moment(date).format(format) : moment(date).calendar()

export default toHumanDate
