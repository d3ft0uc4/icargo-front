export default maybePromise =>
  maybePromise && maybePromise.then
    ? maybePromise
    : Promise.resolve(maybePromise)
