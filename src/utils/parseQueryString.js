import querystring from 'querystring'

const parseQueryString = search => {
  return querystring.parse(search.substr(1))
}

export default parseQueryString
