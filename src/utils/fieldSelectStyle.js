const fieldSelectStyle = {
  option: (styles, { data, isDisabled, isFocused, isSelected }) => ({
    ...styles,
    color: 'black',
    background: isFocused ? 'rgb(238, 238, 238)' : 'white',
    '&:hover': {
      background: 'rgb(238, 238, 238)',
    },
    '&:active': {
      background: 'rgb(238, 238, 238)',
    },
  }),
  control: () => ({
    background: 'white',
    paddingTop: '0px',
    height: '45px',
    lineHeight: '45px',
    paddingLeft: '2px',
    borderRadius: '5px',
    border: 'none',
    fontSize: '15px',
    fontWeight: '100',
  }),
  singleValue: (provided, state) => {
    return { ...provided, height: '45px' }
  },
}

export default fieldSelectStyle
