import find from 'lodash/find'
import $ from 'jquery'
const validateEmail = email => {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

const stripNonDigits = input => {
  return input.replace(/[^\d]/g, '')
}

const unwrapINN = input => {
  const val = stripNonDigits((input || '').toString()).slice(0, 12)
  if (val.length === 12 || val.length === 10) {
    return val
  }
  return ''
}

const unwrapKPP = input => {
  const val = stripNonDigits((input || '').toString()).slice(0, 9)
  if (val.length === 9) {
    return val
  }
  return ''
}
const scrollToTargetAdjusted = target => {
  const headerOffset =
    70 + document.getElementById('progress-bar-wrapper').offsetHeight
  $('html, body').animate(
    { scrollTop: $('#' + target).offset().top - headerOffset },
    'slow'
  )
}

const findByLabel = (values, label) => {
  return find(values, { label })
}
const findByValue = (values, value) => {
  return find(values, { value })
}
const findByRateAirline = (values, rate, airline) => {
  return find(values, { rate, airline })
}
const setValueById = (id, value) => {
  if (document.getElementById(id)) {
    document.getElementById(id).value = value
  }
}
export {
  validateEmail,
  stripNonDigits,
  unwrapINN,
  unwrapKPP,
  scrollToTargetAdjusted,
  findByLabel,
  findByValue,
  findByRateAirline,
  setValueById,
}
