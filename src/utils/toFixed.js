const toFixed = v => {
  if (!v) {
    return '-'
  }
  return numberWithCommas(
    parseFloat(v)
      .toFixed(2)
      .replace('.', ',')
  )
}
const numberWithCommas = x => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
}
export default toFixed
