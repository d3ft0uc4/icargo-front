import moment from 'moment'

const dateToTime = dateStr => {
  const milliseconds = moment(dateStr, 'DD.MM.YYYY').format('x')
  return new Date(milliseconds)
}

const sumParse = sumStr => {
  return parseFloat(sumStr.replace(' ', ''))
}

export default { dateToTime, sumParse }
