import React from 'react'
import { Route, Switch } from 'react-router-dom'
import HomePage from './pages/HomePage'
import NewOrderPage from './pages/NewOrderPage'
import AboutPage from './pages/AboutPage'
import LicPage from './pages/LicPage'
import ContactPage from './pages/ContactPage'
import DashboardPage from './pages/DashboardPage'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.js'
import './assets/css/style.css'

export default function App() {
  return (
    <Switch>
      <Route exact path="/" component={HomePage} />
      <Route path="/new" component={NewOrderPage} />
      <Route path="/about" component={AboutPage} />
      <Route path="/lic" component={LicPage} />
      <Route path="/contact" component={ContactPage} />
      <Route path="/dashboard" component={DashboardPage} />
    </Switch>
  )
}
