import engine from 'store/src/store-engine'
import cookieStorage from 'store/storages/cookieStorage'
const cookieStore = engine.createStore(cookieStorage)
export default cookieStore
