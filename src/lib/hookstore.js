import React from 'react'
import globalHook from 'use-global-hook'

const initialState = {}

const actions = {
  setPhone: (store, phone) => {
    store.setState({ phone: phone })
  },
}

const useGlobal = globalHook(React, initialState, actions)

export default useGlobal
