import engine from 'store/src/store-engine'
import localStorage1 from 'store/storages/localStorage'
const localStore = engine.createStore(localStorage1)
export default localStore
