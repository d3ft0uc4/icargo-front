import WithdrawalTab from '../../components/dashboard/WithdrawalTab'

import React, { useState, useEffect } from 'react'

import HistoryBalance from '../../components/dashboard/HistoryBalance'

const Balance = ({ token, idContact }) => {
  return (
    <>
      <div className="row" style={{ padding: '10px 0' }}>
        <div className="col-lg-7">
          <HistoryBalance token={token} idContact={idContact} />
        </div>
        <div className="col-lg-4 offset-lg-1">
          <WithdrawalTab token={token} idContact={idContact} />
        </div>
      </div>
    </>
  )
}
export default Balance
