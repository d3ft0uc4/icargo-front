import React, { useState, useEffect } from 'react'
import Tab from '../../components/Tab'
import axios from 'axios'
import { GET_USER_DATA, UPDATE_USER_DATA } from '../../lib/backendRoutes'
import FormField from '../../components/FormField'
import InputMask from 'react-input-mask'
import HistoryOrderTable from '../../components/dashboard/HistoryOrderTable'
import AnimatedDiv from '../../components/AnimatedDiv'

const Profile = ({ token, idContact }) => {
  const [blockSettings, setBlockSettings] = useState(true)
  const [profileData, setProfileData] = useState({})
  const [mounted, setMounted] = useState(false)
  useEffect(() => {
    axios
      .post(
        GET_USER_DATA,
        { id_contact: parseInt(idContact) },
        { headers: { token: token } }
      )
      .then(response => {
        setProfileData({
          id_contact: response.data.id_contact,
          name_contact: response.data.name_contact,
          email: response.data.email,
        })
        setMounted(true)
      })
      .catch()
  }, [idContact, token])
  return (
    <AnimatedDiv pose={mounted ? 'visible' : 'hidden'}>
      <Tab
        label="Личные данные"
        initialOpen={true}
        style={{ padding: '10px 0' }}
      >
        <form
          onSubmit={e => {
            e.preventDefault()
            setBlockSettings(true)
            axios
              .post(UPDATE_USER_DATA, profileData, {
                headers: { token: token },
              })
              .then()
              .catch()
          }}
        >
          <>
            <FormField label="Номер телефона">
              <InputMask
                className="form-input"
                placeholder="+7 (999) 999-99-99"
                mask="+7 (999) 999-99-99"
                maskChar={9}
                id="input-phone"
                value={profileData.id_contact}
                alwaysShowMask={true}
                disabled={true}
              />
            </FormField>
            <FormField label="ФИО">
              <>
                <input
                  type="text"
                  className="form-input"
                  required
                  name="fio"
                  value={profileData.name_contact}
                  onInput={e => {
                    setProfileData({
                      ...profileData,
                      name_contact: e.target.value,
                    })
                  }}
                  disabled={blockSettings}
                />
              </>
            </FormField>
            <FormField label="Контактный email">
              <>
                <input
                  type="text"
                  className="form-input"
                  required
                  name="email"
                  value={profileData.email}
                  onInput={e => {
                    setProfileData({
                      ...profileData,
                      email: e.target.value,
                    })
                  }}
                  disabled={blockSettings}
                />
              </>
            </FormField>
            {!blockSettings && (
              <FormField>
                <button
                  type="submit"
                  className="btn calc-btn mt-0"
                  style={{
                    width: '100%',
                  }}
                >
                  Сохранить
                </button>
              </FormField>
            )}
            {blockSettings && (
              <FormField>
                <button
                  type="button"
                  onClick={() => setBlockSettings(!blockSettings)}
                  className="btn calc-btn mt-0"
                  style={{
                    width: '100%',
                  }}
                >
                  Редактировать
                </button>
              </FormField>
            )}
          </>
        </form>
      </Tab>
    </AnimatedDiv>
  )
}
export default Profile
