import React, { useState, useEffect } from 'react'
import Tab from '../../components/Tab'
import axios from 'axios'
import { GET_USER_ORDERS } from '../../lib/backendRoutes'
import HistoryOrderTable from '../../components/dashboard/HistoryOrderTable'
import sortIcon from '../../assets/img/sortIcon.png'
import Select from 'react-select'
import sortSelectStyle from '../../utils/sortSelectStyle'
import { sortOptions, sortOptionsTxs } from '../../utils/sorts'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/fontawesome-free-solid'
import AnimatedDiv from '../../components/AnimatedDiv'
import filterIcon from '../../assets/img/filterIcon.png'
const faStyle = {
  position: 'absolute',
  top: '15px',
  right: '6px',
}
const History = ({ token, idContact }) => {
  const [sortOption, setSortOption] = useState(sortOptions[0])
  const [userOrders, setUserOrders] = useState([])
  const [defaultUserOrders, setDefaultUserOrders] = useState([])
  const [asc, setAsc] = useState(true)
  const sortHistory = (oldVal, newVal) => {
    if (newVal.value === 'default') {
      setUserOrders(Object.assign([], defaultUserOrders))
      setAsc(true)
    } else {
      let s = userOrders.sort(newVal.comparator)
      if (asc && oldVal.value === newVal.value) {
        s = s.reverse()
      }
      setUserOrders(s)
      if (oldVal.value === newVal.value) {
        setAsc(!asc)
      } else {
        setAsc(true)
      }
    }
  }

  const [mounted, setMounted] = useState(false)
  useEffect(() => {
    axios
      .post(
        GET_USER_ORDERS,
        {
          id_contact: parseInt(idContact),
          sort: sortOption.value,
          asc,
          skip: 0,
          limit: 0,
        },
        { headers: { token: token } }
      )
      .then(response => {
        setUserOrders(response.data)
        setDefaultUserOrders(Object.assign([], response.data))
        setMounted(true)
      })
      .catch()
  }, [idContact, token, asc, sortOption])

  return (
    <AnimatedDiv pose={mounted ? 'visible' : 'hidden'}>
      <div style={{ padding: '10px 0' }}>
        <div
          className="mt-1 mb-1"
          style={{ paddingLeft: '0', paddingRight: '0', display: 'flex' }}
        >
          <h5
            className="col-5"
            style={{
              float: 'left',
              paddingTop: '11px',
              fontWeight: 100,
              paddingLeft: 0,
            }}
          >
            История заявок
          </h5>

          <div className="col-1 col-lg-5"> </div>
          <div
            className="col-6 col-lg-2"
            style={{ paddingTop: '3px', paddingRight: 0 }}
          >
            <img
              src={sortIcon}
              alt=""
              width="15"
              height="15"
              style={{ position: 'absolute', top: '12px', left: '-10px' }}
            />
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
              styles={sortSelectStyle}
              name="client_type"
              required
              id="client_type"
              defaultValue={sortOptions[0]}
              value={sortOption}
              onChange={e => {
                if (e === sortOption) {
                  setAsc(!asc)
                } else {
                  setAsc(true)
                  setSortOption(e)
                }
              }}
              isDisabled={false}
              options={sortOptions}
            />
            <FontAwesomeIcon
              icon={faAngleDown}
              className={'input-angle orange'}
              style={faStyle}
              size="sm"
            />
          </div>
        </div>
        <div>
          <div>
            <HistoryOrderTable data={userOrders} />
          </div>
        </div>
      </div>
    </AnimatedDiv>
  )
}
export default History
