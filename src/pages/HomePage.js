import React, { useEffect, useState } from 'react'
import Layout from '../components/Layout'

import About from '../components/About'
import axios from 'axios'
import { GET_ROUTES, CALC_RATES, TABLE_RATES } from '../lib/backendRoutes'
import uniqBy from 'lodash/uniqBy'
import Autocomplete from 'react-autocomplete'
import RateTable from '../components/RateTable'
import Highlighter from 'react-highlight-words'

const HomePage = props => {
  useEffect(() => {
    const hash = props.history.location.hash
    if (hash && document.getElementById(hash.substr(1))) {
      // Check if there is a hash and if an element with that id exists
      setTimeout(() => {
        document
          .getElementById(hash.substr(1))
          .scrollIntoView({ behavior: 'smooth' })
      }, 100)
    }
  }, [props.history.location.hash]) // Fires every time hash changes
  const matchStocks = (state, value) => {
    return (
      (
        state.city_name.toString().toLowerCase() +
        ', ' +
        state.airport_name.toString().toLowerCase()
      ).indexOf(value.toLowerCase()) !== -1 ||
      state.city_name
        .toString()
        .toLowerCase()
        .indexOf(value.toLowerCase()) !== -1 ||
      state.airport_name
        .toString()
        .toLowerCase()
        .indexOf(value.toLowerCase()) !== -1 ||
      state.airport_code_iata
        .toString()
        .toLowerCase()
        .indexOf(value.toLowerCase()) !== -1
    )
  }
  const [routes, setRoutes] = useState([])
  const [routesFrom, setRoutesFrom] = useState([])
  const [codeFrom, setCodeFrom] = useState('')
  const [valueFrom, setValueFrom] = useState('')
  const [routesTo, setRoutesTo] = useState([])
  const [codeTo, setCodeTo] = useState([])
  const [valueTo, setValueTo] = useState('')
  const [tableRates, setTableRates] = useState([])
  useEffect(() => {
    const getRoutes = async () => {
      const { data } = await axios.get(GET_ROUTES)
      return data
    }
    getRoutes().then(d => {
      setRoutes(d)
      setRoutesFrom(uniqBy(d.map(r => r.from), o => o.id_airport))
    })
  }, [])
  useEffect(() => {
    setCodeTo('')
    setValueTo('')
    setRoutesTo(
      uniqBy(
        routes
          .filter(i => i.from.airport_code_iata === codeFrom)
          .map(r => r.to),
        o => o.id_airport
      )
    )
  }, [codeFrom, routes])
  useEffect(() => {
    const getTableRates = async () => {
      const { data } = await axios.get(TABLE_RATES)
      return data
    }
    getTableRates().then(d => {
      setTableRates(
        d.rates.map((item, i) => [
          i + 1,
          item.airport_from.city_name +
            ' (' +
            item.airport_from.airport_code_iata +
            ')',
          item.airport_to.city_name +
            ' (' +
            item.airport_to.airport_code_iata +
            ')',
          item.airline.airline_name + ' (' + item.airline.airline_code + ')',
          item.min_weight,
          item.rates.less50,
          item.rates['100'],
          item.rates['300'],
          item.rates['500'],
          item.rates['1000'],
          item.rates.over1000,
        ])
      )
    })
  }, [])
  return (
    <Layout>
      <div className="row" style={{ marginTop: '80px' }}>
        <div className="col-lg-12 text-center">
          <h1 className="mt-5 pt-3">Заполни заявку</h1>
          <p className="lead">
            И получи скидку на отправку груза
            <br />{' '}
            <span style={{ color: '#FF9600', borderBottom: '1px solid' }}>
              2 руб/кг
            </span>{' '}
            или кэшбек на карту
          </p>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <form
            style={{ width: '100%' }}
            onSubmit={async e => {
              e.preventDefault()
              const w = e.target.weight.value
              const input = {
                id_airport_from: routes
                  .filter(i => i.from.airport_code_iata === codeFrom)
                  .map(r => r.from)[0].id_airport,
                id_airport_to: routes
                  .filter(i => i.to.airport_code_iata === codeTo)
                  .map(r => r.to)[0].id_airport,
                id_type_cargo: 100,
                weight: w,
              }
              const { data } = await axios.post(CALC_RATES, input)
              props.history.push({
                pathname: '/new',
                state: {
                  data: data,
                  valueFrom,
                  valueTo,
                  codeFrom,
                  codeTo,
                  weight: w,
                  ...input,
                  id_order: data.order.id,
                },
              })
            }}
          >
            <div className="form-row">
              <div className="form-group col-md-4">
                <Autocomplete
                  value={valueFrom}
                  inputProps={{
                    id: 'states-autocomplete',
                    className: 'form-control search-input',
                    required: true,
                    style: { paddingLeft: '15px' },
                    onBlur: () => {
                      if (!codeFrom) {
                        setValueFrom('')
                      }
                    },
                  }}
                  wrapperStyle={{}}
                  items={routesFrom}
                  getItemValue={item => item.airport_name}
                  shouldItemRender={matchStocks}
                  onChange={(event, value) => {
                    setCodeFrom('')
                    setValueFrom(value.toString())
                  }}
                  onSelect={(value, item) => {
                    setValueFrom(
                      item.city_name.toString() + ', ' + value.toString()
                    )
                    setCodeFrom(item.airport_code_iata)
                  }}
                  renderMenu={children => (
                    <div className="from-variants">{children}</div>
                  )}
                  renderInput={props => (
                    <>
                      <input {...props} />
                      <span className="floating-label">Пункт вылета</span>
                      <p
                        style={{
                          position: 'absolute',
                          color: 'black',
                          right: '20px',
                          top: '20px',
                        }}
                      >
                        <span className="airport-code">{codeFrom}</span>
                      </p>
                    </>
                  )}
                  renderItem={(item, highlighted) => {
                    return (
                      <div
                        key={item.id_airport.toString()}
                        style={{
                          backgroundColor: highlighted ? '#eee' : 'transparent',
                        }}
                      >
                        <p>
                          <span className="airport-city">
                            <Highlighter
                              highlightClassName="orange"
                              searchWords={[valueFrom]}
                              autoEscape={true}
                              textToHighlight={`${item.city_name.toString()}, ${item.airport_name.toString()}`}
                            />
                          </span>
                        </p>
                        <p>
                          <span className="airport-code">
                            {item.airport_code_iata.toString()}
                          </span>
                        </p>
                      </div>
                    )
                  }}
                />
              </div>
              <div className="form-group col-md-4">
                <Autocomplete
                  value={valueTo}
                  inputProps={{
                    id: 'states-autocomplete',
                    className: 'form-control search-input',
                    required: true,
                    style: { paddingLeft: '15px' },
                    disabled: !codeFrom,
                    onBlur: () => {
                      if (!codeTo) {
                        setValueTo('')
                      }
                    },
                  }}
                  wrapperStyle={{}}
                  items={routesTo}
                  getItemValue={item => item.airport_name}
                  shouldItemRender={matchStocks}
                  onChange={(event, value) => {
                    setCodeTo('')
                    setValueTo(value.toString())
                  }}
                  onSelect={(value, item) => {
                    setValueTo(
                      item.city_name.toString() + ', ' + value.toString()
                    )
                    setCodeTo(item.airport_code_iata)
                  }}
                  renderMenu={children => (
                    <div className="from-variants">{children}</div>
                  )}
                  renderInput={props => (
                    <>
                      <input {...props} />
                      <span className="floating-label">Пункт назначения</span>
                      <p
                        style={{
                          position: 'absolute',
                          color: 'black',
                          right: '20px',
                          top: '20px',
                        }}
                      >
                        <span className="airport-code">{codeTo}</span>
                      </p>
                    </>
                  )}
                  renderItem={(item, highlighted) => (
                    <div
                      key={item.id_airport.toString()}
                      style={{
                        backgroundColor: highlighted ? '#eee' : 'transparent',
                      }}
                    >
                      <p>
                        <span className="airport-city">
                          <Highlighter
                            highlightClassName="orange"
                            searchWords={[valueTo]}
                            autoEscape={true}
                            textToHighlight={`${item.city_name.toString()}, ${item.airport_name.toString()}`}
                          />
                        </span>
                      </p>
                      <p>
                        <span className="airport-code">
                          {item.airport_code_iata.toString()}
                        </span>
                      </p>
                    </div>
                  )}
                />
              </div>
              <div className="form-group col-md-2">
                <input
                  type="number"
                  step="any"
                  name="weight"
                  className="form-control search-input"
                  min="0.01"
                  required
                />
                <span className="floating-label">Вес партии, кг.</span>
              </div>
              <div className="form-group col-md-2">
                <button
                  type="submit"
                  className="btn calc-btn"
                  style={{
                    width: '100%',
                  }}
                >
                  Рассчитать
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
      <RateTable data={tableRates} className="mt-5" />
      <About />
    </Layout>
  )
}

export default HomePage
