import React, { useState } from 'react'
import Layout from '../components/Layout'
import DashboardTab from '../components/DashboardTab'

const ContactPage = () => {
  const [active, setActive] = useState('contact')
  window.scrollTo(0, 0)
  return (
    <Layout>
      <div className="navbar navbar-expand-lg background-header dashboard-header-wrapper">
        <div className="container" style={{ padding: 0 }}>
          <p style={{ fontSize: '21px', marginBottom: 0 }}>Информация</p>
          <button
            className="navbar-toggle navbar-toggler  navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive2"
            aria-controls="navbarResponsive2"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon icon-bar" />
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive2">
            <ul className="navbar-nav ml-auto dashboard-header">
              <li className="nav-item dashboard-item">
                <a
                  href="#"
                  className={'nav-link ' + (active === 'about' ? 'about' : '')}
                  onClick={() => setActive('about')}
                >
                  О сервисе
                </a>
              </li>
              <li className="nav-item dashboard-item">
                <a
                  href="#"
                  className={
                    'nav-link ' + (active === 'contact' ? 'orange' : '')
                  }
                  onClick={() => setActive('contact')}
                >
                  Контакты
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <DashboardTab active={active === 'about'}>1</DashboardTab>
      <DashboardTab active={active === 'contact'}>
        <div className="row">
          <div className="col-lg-12 mt-1 mb-1">
            <h5
              className="orange"
              style={{ float: 'left', paddingTop: '11px', fontWeight: 100 }}
            >
              Контакты
            </h5>
          </div>
        </div>
        <div style={{ fontSize: '14px' }}>
          <div className="row">
            <div className="col-lg-3">
              <p>Полное официальное компании</p>
            </div>
            <div className="col-lg-5">
              <p>Общество с ограниченной ответственностью «КаргоБукинг»</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>Сокращенное наименование компании </p>
            </div>
            <div className="col-lg-5">
              <p>ООО «КаргоБукинг»</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>Генеральный директор</p>
            </div>
            <div className="col-lg-5">
              <p>Прокофьев Иван Юрьевич</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>ИНН</p>
            </div>
            <div className="col-lg-5">
              <p>5047235102</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>КПП</p>
            </div>
            <div className="col-lg-5">
              <p>504701001</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>ОГРН</p>
            </div>
            <div className="col-lg-5">
              <p>1195081083625</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>Юридический адрес</p>
            </div>
            <div className="col-lg-5">
              <p>
                141402, Московская обл., г. Химки, Международное ш., владение 1,
                офис 1205
              </p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>Почтовый адрес</p>
            </div>
            <div className="col-lg-5">
              <p>141425, Московская обл., Шереметьево-2, а/я 174</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>Телефон</p>
            </div>
            <div className="col-lg-5 orange">
              <p>8 800 600 51 28</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>Наименование банка</p>
            </div>
            <div className="col-lg-5">
              <p>ПАО Сбербанк</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>БИК</p>
            </div>
            <div className="col-lg-5">
              <p>044525225</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>Номер расчетного счета</p>
            </div>
            <div className="col-lg-5">
              <p>40702810440000068528</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>Корреспондентский счет банка</p>
            </div>
            <div className="col-lg-5">
              <p>30101810400000000225</p>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3">
              <p>E-mail</p>
            </div>
            <div className="col-lg-5">
              <a
                href="mailto:info@cargobooking.ru"
                target="_blank"
                className="orange hover"
              >
                info@cargobooking.ru
              </a>
            </div>
          </div>
        </div>
      </DashboardTab>
    </Layout>
  )
}

export default ContactPage
