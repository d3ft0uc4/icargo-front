/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState, useCallback } from 'react'
import Layout from '../components/Layout'
import Spinner from '../components/Spinner'
import axios from 'axios'
import { GET_ORDER } from '../lib/backendRoutes'

import Progressbar from '../components/Progressbar'
import StepTab from '../components/StepTab'
import { Redirect } from 'react-router'
import Cookies from 'universal-cookie'
import AnimatedDiv from '../components/AnimatedDiv'
import SuccessOrderModal from './new_order/SuccessOrderModal'
import getProgressStep from '../utils/getProgressStep'

import Step1 from './new_order/Step1'
import Step2 from './new_order/Step2'
import Step3 from './new_order/Step3'
import Step4 from './new_order/Step4'
import Step5 from './new_order/Step5'
import Step6 from './new_order/Step6'
import Step7 from './new_order/Step7'
import StepAccept from './new_order/StepAccept'

const NewOrderPage = props => {
  const cookies = new Cookies()

  const token = cookies.get('token')
  const idContact = cookies.get('id_contact')
  const [redirect, setRedirect] = useState(false)
  const [initialOrder, setInitialOrder] = useState({})
  const [mounted, setMounted] = useState(false)

  const state = props.location.state || {}
  const [loading, setLoading] = useState(false)
  const [successOrder, setSuccessOrder] = useState(false)
  const [progress, setProgress] = useState(0)
  const [initialProgress, setInitialProgress] = useState(0)
  const forceShow = false

  useEffect(() => {
    window.scrollTo(0, 0)
    axios
      .post(
        GET_ORDER,
        { id_order: parseInt(state.id_order) },
        { headers: { token: token } }
      )
      .then(response => {
        setInitialOrder(response.data)
        setProgress(getProgressStep(response.data.id_state))
        setInitialProgress(getProgressStep(response.data.id_state))
        setMounted(true)
      })
      .catch(() => setRedirect(true))
  }, [])

  if (redirect) {
    return <Redirect to="/" />
  }
  return (
    <React.Fragment>
      <Spinner enabled={loading} />
      <Layout>
        <AnimatedDiv pose={mounted ? 'visible' : 'hidden'}>
          <Progressbar
            progress={parseInt((initialProgress * 100) / 8)}
            date={initialOrder.date_add}
            order={state.id_order}
            className="progress-bar-wrapper"
          />
          <div className="new-order-content">
            <StepTab
              id="tab1"
              label="Основные параметры груза"
              labelRight={'Шаг 1 из 8'}
              forceShow={forceShow}
              current={0}
              progress={progress}
              setProgress={setProgress}
              initialProgress={initialProgress}
              setInitialProgress={setInitialProgress}
            >
              {(progress === 0 || forceShow) && (
                <Step1
                  token={token}
                  idOrder={state.id_order}
                  state={state}
                  progress={progress}
                  setProgress={setProgress}
                  initialProgress={initialProgress}
                  setInitialProgress={setInitialProgress}
                />
              )}
            </StepTab>
            <StepTab
              id="tab2"
              label="Клиент"
              labelRight={'Шаг 2 из 8'}
              forceShow={forceShow}
              current={1}
              progress={progress}
              setProgress={setProgress}
              initialProgress={initialProgress}
              setInitialProgress={setInitialProgress}
            >
              {(progress === 1 || forceShow) && (
                <Step2
                  token={token}
                  idOrder={state.id_order}
                  idContact={idContact}
                  progress={progress}
                  setProgress={setProgress}
                  initialProgress={initialProgress}
                  setInitialProgress={setInitialProgress}
                />
              )}
            </StepTab>
            <StepTab
              id="tab3"
              label="Дополнительные параметры груза"
              labelRight={'Шаг 3 из 8'}
              forceShow={forceShow}
              current={2}
              progress={progress}
              setProgress={setProgress}
              initialProgress={initialProgress}
              setInitialProgress={setInitialProgress}
            >
              {(progress === 2 || forceShow) && (
                <Step3
                  token={token}
                  idOrder={state.id_order}
                  progress={progress}
                  setProgress={setProgress}
                  initialProgress={initialProgress}
                  setInitialProgress={setInitialProgress}
                />
              )}
            </StepTab>
            <StepTab
              id="tab4"
              label="Грузоотправитель"
              labelRight={'Шаг 4 из 8'}
              forceShow={forceShow}
              current={3}
              progress={progress}
              setProgress={setProgress}
              initialProgress={initialProgress}
              setInitialProgress={setInitialProgress}
            >
              {(progress === 3 || forceShow) && (
                <Step4
                  token={token}
                  idOrder={state.id_order}
                  progress={progress}
                  setProgress={setProgress}
                  initialProgress={initialProgress}
                  setInitialProgress={setInitialProgress}
                />
              )}
            </StepTab>
            <StepTab
              id="tab5"
              label="Доставка груза до аэропорта"
              labelRight={'Шаг 5 из 8'}
              forceShow={forceShow}
              current={4}
              progress={progress}
              setProgress={setProgress}
              initialProgress={initialProgress}
              setInitialProgress={setInitialProgress}
            >
              {(progress === 4 || forceShow) && (
                <Step5
                  token={token}
                  idOrder={state.id_order}
                  progress={progress}
                  setProgress={setProgress}
                  initialProgress={initialProgress}
                  setInitialProgress={setInitialProgress}
                />
              )}
            </StepTab>
            <StepTab
              id="tab6"
              label="Грузополучатель"
              labelRight={'Шаг 6 из 8'}
              forceShow={forceShow}
              current={5}
              progress={progress}
              setProgress={setProgress}
              initialProgress={initialProgress}
              setInitialProgress={setInitialProgress}
            >
              {(progress === 5 || forceShow) && (
                <Step6
                  idOrder={state.id_order}
                  token={token}
                  progress={progress}
                  setProgress={setProgress}
                  initialProgress={initialProgress}
                  setInitialProgress={setInitialProgress}
                />
              )}
            </StepTab>
            <StepTab
              id="tab7"
              label="Страхование груза"
              labelRight={'Шаг 7 из 8'}
              forceShow={forceShow}
              current={6}
              progress={progress}
              setProgress={setProgress}
              initialProgress={initialProgress}
              setInitialProgress={setInitialProgress}
            >
              {(progress === 6 || forceShow) && (
                <Step7
                  token={token}
                  setProgress={setProgress}
                  idOrder={state.id_order}
                  progress={progress}
                  initialProgress={initialProgress}
                  setInitialProgress={setInitialProgress}
                />
              )}
            </StepTab>
            <StepTab
              id="tab8"
              label="Подтверждение заявки"
              labelRight={'Шаг 8 из 8'}
              forceShow={forceShow}
              current={7}
              progress={progress}
              setProgress={setProgress}
              initialProgress={initialProgress}
              setInitialProgress={setInitialProgress}
            >
              {(progress === 7 || forceShow) && (
                <StepAccept
                  idOrder={state.id_order}
                  token={token}
                  setSuccessOrder={setSuccessOrder}
                  setLoading={setLoading}
                  progress={progress}
                  setProgress={setProgress}
                  initialProgress={initialProgress}
                  setInitialProgress={setInitialProgress}
                />
              )}
            </StepTab>
          </div>
        </AnimatedDiv>

        <SuccessOrderModal
          isOpen={successOrder}
          idOrder={state.id_order}
          token={token}
        />
      </Layout>
    </React.Fragment>
  )
}

export default NewOrderPage
