/* eslint-disable react-hooks/exhaustive-deps */
import axios from 'axios'
import { CHECK_INN, CHECK_PASSPORT, STEP4 } from '../../lib/backendRoutes'
import FormField from '../../components/FormField'
import React, { useState, useEffect } from 'react'
import { scrollToTargetAdjusted, unwrapINN } from '../../utils/misc'
import InputMask from 'react-input-mask'
import getOrder from '../../utils/getOrder'

const Step4 = ({
  token,
  setProgress,
  idOrder,
  progress,
  initialProgress,
  setInitialProgress,
}) => {
  const [sameAs2, setSameAs2] = useState(undefined)
  const [error, setError] = useState('')
  const [inputClientType, setInputClientType] = useState('')
  const [checkFlag, setCheckFlag] = useState(progress < initialProgress)
  const [INN, setINN] = useState('')
  const [companyName, setCompanyName] = useState('')
  const [companyAddress, setCompanyAddress] = useState('')
  const [passport, setPassport] = useState('')
  const [FIO, setFIO] = useState('')
  const [address, setAddress] = useState('')
  const [contactPhone, setContactPhone] = useState('')
  const [mounted, setMounted] = useState(false)
  const [order, setOrder] = useState({})
  const stepFieldHelper = (flag, o = undefined) => {
    const ord = order || o
    if (flag) {
      setCheckFlag(true)
      setInputClientType(ord.customer_type)
      setINN(ord.customer_id)
      setCompanyName(ord.customer_name)
      setCompanyAddress(ord.customer_address)
      setPassport(ord.customer_id)
      setFIO(ord.customer_name)
      setAddress(ord.customer_address)
      setContactPhone(ord.customer_phone)
    } else {
      setInputClientType('')
      setINN('')
      setCompanyName('')
      setCompanyAddress('')
      setPassport('')
      setFIO('')
      setAddress('')
      setContactPhone('')
    }
  }
  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        setOrder(data)
        if (progress < initialProgress) {
          if (data.sender_id === data.customer_id && data.sender_id) {
            setSameAs2(true)
            setInputClientType(data.customer_type)
            setINN((data.customer_id || '').toString())
            setCompanyName(data.customer_name)
            setCompanyAddress(data.customer_address)
            setPassport((data.customer_id || '').toString())
            setFIO(data.customer_name)
            setAddress(data.customer_address)
            setContactPhone(data.customer_phone)
          } else {
            setSameAs2(false)
            setInputClientType(data.sender_type)
            setINN((data.sender_id || '').toString())
            setCompanyName(data.sender_name)
            setCompanyAddress(data.sender_address)
            setPassport((data.sender_id || '').toString())
            setFIO(data.sender_name)
            setAddress(data.sender_address)
            setContactPhone(data.sender_phone)
          }
        }
        setMounted(true)
      })
      .catch()
  }, [idOrder, token])
  return (
    mounted && (
      <form
        onSubmit={e => {
          e.preventDefault()
          const formValid =
            checkFlag &&
            idOrder &&
            inputClientType &&
            (inputClientType === 'UL' ? unwrapINN(INN) : passport) &&
            (inputClientType === 'UL' ? companyName : FIO) &&
            (inputClientType === 'UL' ? companyAddress : address) &&
            contactPhone
          if (!formValid) {
            setError('Заполните обязательные поля')
          } else {
            axios
              .put(
                STEP4,
                {
                  id_order: idOrder,
                  order_sender_type: inputClientType,
                  order_sender_id:
                    inputClientType === 'UL' ? unwrapINN(INN) : passport,
                  order_sender_name:
                    inputClientType === 'UL' ? companyName : FIO,
                  order_sender_address:
                    inputClientType === 'UL' ? companyAddress : address,
                  order_sender_phone: (contactPhone || '').toString(),
                },
                { headers: { token: token } }
              )
              .then(() => {
                setProgress(4)
                if (initialProgress < 4) {
                  setInitialProgress(4)
                }
                scrollToTargetAdjusted('tab5')
              })
          }
        }}
      >
        <FormField label="Клиент является грузоотправителем?" required={true}>
          <div className="radio-button-group">
            <label>
              <input
                type="radio"
                value={'1'}
                name={'sameAs2'}
                checked={sameAs2 === true}
                onChange={e => {
                  setSameAs2(true)
                  stepFieldHelper(true)
                  setCheckFlag(true)
                }}
              />
              <span>{'Да'}</span>
            </label>
            <label>
              <input
                type="radio"
                value={'0'}
                name={'is_same_as_2'}
                checked={sameAs2 === false}
                onChange={e => {
                  setSameAs2(false)
                  stepFieldHelper(false)
                  setCheckFlag(false)
                }}
              />
              <span>{'Нет'}</span>
            </label>
          </div>
        </FormField>
        <FormField
          label="Тип грузоотправителя"
          required={true}
          style={{
            display: sameAs2 === false ? 'flex' : 'none',
          }}
        >
          <div className="radio-button-group">
            <label>
              <input
                type="radio"
                value={'FL'}
                name={'client_type'}
                checked={inputClientType === 'FL'}
                onChange={e => {
                  setCheckFlag(false)
                  stepFieldHelper(false)
                  setInputClientType('FL')
                }}
              />
              <span>{'Физ. лицо'}</span>
            </label>
            <label>
              <input
                type="radio"
                value={'UL'}
                name={'client_type'}
                checked={inputClientType === 'UL'}
                onChange={e => {
                  setCheckFlag(false)
                  stepFieldHelper(false)
                  setInputClientType('UL')
                }}
              />
              <span>{'Юр. лицо'}</span>
            </label>
          </div>
        </FormField>

        <FormField
          label="ИНН грузоотправителя"
          required={true}
          style={{
            display: inputClientType === 'UL' ? 'flex' : 'none',
          }}
        >
          <InputMask
            className="form-input"
            disabled={sameAs2}
            placeholder="999999999999"
            mask="999999999999"
            maskChar={null}
            value={INN}
            onFocus={e => setError('')}
            onInput={e => {
              setINN(e.target.value)
              setCheckFlag(false)
              const inp = e.target.value.replace(/\D/g, '').slice(0, 12)
              if (inp.length === 10 || inp.length === 12) {
                axios
                  .post(CHECK_INN, {
                    id_order: idOrder,
                    inn: inp,
                  })
                  .then(d => {
                    if (d.data.status == 1) {
                      const client = d.data.data
                      setINN(client.id_client)
                      setCompanyAddress(client.address)
                      setContactPhone(client.phone_client)
                      setCompanyName(client.name)
                      setCheckFlag(true)
                    } else {
                      setError('Ничего не найдено')
                    }
                  })
              }
            }}
          />
        </FormField>
        <FormField
          label="Наименование грузоотправителя"
          required={true}
          style={{
            display: inputClientType === 'UL' && checkFlag ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            className="form-input"
            disabled={sameAs2 || true}
            placeholder={'OOO "КаргоБукинг"'}
            value={companyName}
            onInput={e => setCompanyName(e.target.value)}
          />
        </FormField>
        <FormField
          label="Юр. адрес грузоотправителя"
          required={true}
          style={{
            display: inputClientType === 'UL' && checkFlag ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            disabled={sameAs2 || true}
            className="form-input"
            placeholder={'141700, г. Химки, ш. Международное, вл. 1'}
            value={companyAddress}
            onInput={e => setCompanyAddress(e.target.value)}
          />
        </FormField>
        {inputClientType === 'FL' && (
          <FormField
            label="Номер паспорта грузоотправителя"
            required={true}
            style={{}}
          >
            <InputMask
              className="form-input"
              placeholder="____ ______"
              mask="9999 999999"
              maskChar={null}
              disabled={sameAs2}
              value={passport}
              onFocus={e => setError('')}
              onInput={e => {
                setPassport(e.target.value)
                const inp = e.target.value.replace(/\D/g, '').slice(0, 10)
                setCheckFlag(false)
                if (inp.length === 10) {
                  axios
                    .post(CHECK_PASSPORT, {
                      id_order: idOrder,
                      passport: inp,
                    })
                    .then(d => {
                      if (d.data.status == 1) {
                        const client = d.data.data
                        setPassport(client.id_client)
                        setAddress(client.address)
                        setContactPhone(
                          (client.phone_client || '')
                            .toString()
                            .replace(/\D/g, '')
                        )
                        setFIO(client.name)
                        setCheckFlag(true)
                      } else {
                        setError('Ничего не найдено')
                      }
                    })
                }
              }}
            />
          </FormField>
        )}
        <FormField
          label="ФИО грузоотправителя"
          required={true}
          style={{
            display: inputClientType === 'FL' && checkFlag ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            disabled={sameAs2}
            className="form-input"
            placeholder={'Иванов Иван Иванович'}
            value={FIO}
            onInput={e => setFIO(e.target.value)}
          />
        </FormField>
        <FormField
          label="Адрес проживания грузоотправителя"
          required={true}
          style={{
            display: inputClientType === 'FL' && checkFlag ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            disabled={sameAs2}
            className="form-input"
            placeholder={'МО, г Химки, ш Международное, вл 1'}
            value={address}
            onInput={e => setAddress(e.target.value)}
          />
        </FormField>
        <FormField
          label="Телефон грузоотправителя"
          required={true}
          style={{
            display: !!inputClientType && checkFlag ? 'flex' : 'none',
          }}
        >
          <InputMask
            className="form-input"
            placeholder="+7 (999) 999-99-99"
            mask="+7 (999) 999-99-99"
            id="input-phone"
            maskChar={null}
            disabled={sameAs2}
            value={contactPhone}
            onInput={e => setContactPhone(e.target.value)}
          />
        </FormField>

        {error && (
          <FormField label="">
            <p className={'white'}>{error}</p>
          </FormField>
        )}
        <FormField label="">
          <p className={'orange mt-0'}>
            Проверьте введённые поля и для продолжения подтвердите данные
          </p>
        </FormField>
        <FormField>
          <button
            type="submit"
            className="btn calc-btn mt-0"
            style={{
              width: '100%',
            }}
          >
            Подтвердить
          </button>
        </FormField>
      </form>
    )
  )
}
export default Step4
