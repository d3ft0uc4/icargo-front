import axios from 'axios'
import { STEP7 } from '../../lib/backendRoutes'
import FormField from '../../components/FormField'
import React, { useState, useEffect } from 'react'
import getOrder from '../../utils/getOrder'
import toFixed from '../../utils/toFixed'
import { scrollToTargetAdjusted } from '../../utils/misc'

const Step7 = ({
  token,
  setProgress,
  idOrder,
  setInitialProgress,
  initialProgress,
}) => {
  const [mounted, setMounted] = useState(false)
  const [insured, setInsured] = useState(false)
  const [needInsurance, setNeedInsurance] = useState(false)
  const [insuranceDetails, setInsuranceDetails] = useState('')
  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        setMounted(true)
        document.getElementById('cost').value = data.cargo_insurance_value
        setNeedInsurance(!!data.need_insurance)
        setInsured(!!data.cargo_insurance_details)
        setInsuranceDetails(data.cargo_insurance_details)
      })
      .catch()
  }, [idOrder, token])
  return (
    mounted && (
      <form
        className="mb-4"
        onSubmit={e => {
          e.preventDefault()
          axios
            .put(
              STEP7,
              {
                id_order: idOrder,
                order_cargo_value: document.getElementById('cost').value || '0',
                order_cargo_insurance_details: insuranceDetails,
                order_insurance_need: needInsurance,
              },
              { headers: { token: token } }
            )
            .then(() => {
              setProgress(7)
              if (initialProgress < 7) {
                setInitialProgress(7)
              }
              scrollToTargetAdjusted('tab8')
            })
        }}
      >
        <>
          <FormField label="Стоимость груза, руб" required={false} name="cost">
            <input
              disabled={false}
              type="number"
              step="any"
              name="cost"
              id="cost"
              min="0"
              placeholder="50 000"
              className="form-input"
            />
          </FormField>
          <FormField label="Груз застрахован?" required={false}>
            <div className="radio-button-group">
              <label>
                <input
                  type="radio"
                  checked={insured}
                  onChange={e => setInsured(true)}
                  name={'insured'}
                />
                <span>{'Да'}</span>
              </label>
              <label>
                <input
                  type="radio"
                  checked={!insured}
                  onChange={e => setInsured(false)}
                  name={'insured'}
                />
                <span>{'Нет'}</span>
              </label>
            </div>
          </FormField>
          <FormField
            label="Требуется страховка?"
            required={false}
            hide={insured}
          >
            <div className="radio-button-group">
              <label>
                <input
                  type="radio"
                  checked={needInsurance}
                  onChange={e => setNeedInsurance(true)}
                  name={'need_insurance'}
                />
                <span>{'Да'}</span>
              </label>
              <label>
                <input
                  type="radio"
                  checked={!needInsurance}
                  onChange={e => setNeedInsurance(false)}
                  name={'need_insurance'}
                />
                <span>{'Нет'}</span>
              </label>
            </div>
          </FormField>
          <FormField
            label="Подробное описание условий страхования"
            required={true}
            hide={!insured}
          >
            <textarea
              disabled={false}
              rows={4}
              required={true}
              className="form-input"
              value={insuranceDetails}
              onChange={e => setInsuranceDetails(e.target.value)}
              style={{
                height: '90px',
                lineHeight: '20px',
                resize: 'none',
                fontSize: '16px',
                padding: '15px',
              }}
            />
          </FormField>
          <FormField label="">
            <p className={'orange mt-0'}>
              Проверьте введённые поля и для продолжения подтвердите данные
            </p>
          </FormField>
          <FormField>
            <button
              type="submit"
              className="btn calc-btn mt-0"
              style={{
                width: '100%',
              }}
            >
              Завершить
            </button>
          </FormField>
        </>
      </form>
    )
  )
}
export default Step7
