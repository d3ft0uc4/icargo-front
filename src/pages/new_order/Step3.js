/* eslint-disable react-hooks/exhaustive-deps */
import axios from 'axios'
import { STEP3 } from '../../lib/backendRoutes'
import FormField from '../../components/FormField'
import React, { useState, useEffect } from 'react'
import {
  findByLabel,
  findByValue,
  scrollToTargetAdjusted,
  setValueById,
} from '../../utils/misc'
import Select from 'react-select'
import fieldSelectStyle from '../../utils/fieldSelectStyle'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/fontawesome-free-solid'
import getOrder from '../../utils/getOrder'

const cargoPacks = [
  {
    label: 'Коробки',
    value: '1',
  },
  {
    label: 'Ящики',
    value: '2',
  },
  {
    label: 'Палеты',
    value: '3',
  },
  {
    label: 'Другое',
    value: '4',
  },
]

const Step3 = ({
  token,
  setProgress,
  idOrder,
  initialProgress,
  setInitialProgress,
}) => {
  const [mounted, setMounted] = useState(false)
  const [weight, setWeight] = useState('0')
  const [error, setError] = useState('')
  const [weightSlots, setWeightSlots] = useState(1)
  const [weightSlots2, setWeightSlots2] = useState(1)
  const [cargoPack, setCargoPack] = useState(cargoPacks[0])
  const [rowsFilled, setRowsFilled] = useState({ 0: true })
  const [payedWeight, setPayedWeight] = useState('')
  const checkRow = k => {
    const filled = !!(
      document.getElementById(`cargo_size_${k}_l`).value &&
      document.getElementById(`cargo_size_${k}_w`).value &&
      document.getElementById(`cargo_size_${k}_h`).value &&
      document.getElementById(`cargo_weight_${k}`).value
    )
    const t = Object.assign({}, rowsFilled)
    t[k + 1] = filled
    if (!filled) {
      for (let step = k + 1; step <= 51; step++) {
        t[step] = filled
      }
    }
    t[0] = true
    setRowsFilled(t)
  }

  const calcStep3 = () => {
    let v = 0
    let m = 0
    try {
      for (const k of [...Array(weightSlots2).keys()]) {
        let dims = [
          document.getElementById(`cargo_size_${k}_l`).value,
          document.getElementById(`cargo_size_${k}_w`).value,
          document.getElementById(`cargo_size_${k}_h`).value,
        ]
        v +=
          parseFloat(dims[0]) *
            parseFloat(dims[1]) *
            parseFloat(dims[2]) *
            1.0e-6 || 0
        m += parseFloat(document.getElementById(`cargo_weight_${k}`).value) || 0
      }
      document.getElementById('cargo_size_total_calc').value = v.toFixed(2)
      document.getElementById('cargo_weight_total_calc').value = m
      document.getElementById('cargo_weight_volumed_calc').value = (
        v * 167
      ).toFixed(2)

      document.getElementById('cargo_payed').value = Math.max(
        ...[m, parseFloat((v * 167).toFixed(2))]
      )
      setPayedWeight(document.getElementById('cargo_payed').value)
    } catch (e) {
      document.getElementById('cargo_size_total_calc').value = 0
      document.getElementById('cargo_weight_total_calc').value = 0
      document.getElementById('cargo_weight_volumed_calc').value = 0
    }
  }
  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        setMounted(true)
        setWeight(data.cargo_weight)
        if (data.cargo_type_pack) {
          setWeightSlots(data.cargo_number_pieces)
          setWeightSlots2(data.cargo_number_pieces)
          document.getElementById('package_name').value = data.cargo_name
          setCargoPack(findByLabel(cargoPacks, data.cargo_type_pack))
          const t = Object.assign({}, rowsFilled)
          data.order_cargo_peace_data.map(({ num, size, weight }, k) => {
            const dims = size.split('x')
            setValueById(`cargo_size_${k}_l`, dims[0])
            setValueById(`cargo_size_${k}_w`, dims[1])
            setValueById(`cargo_size_${k}_h`, dims[2])
            setValueById(`cargo_weight_${k}`, weight)
            t[k] = true
          })
          setRowsFilled(t)
          calcStep3()
        }
      })
      .catch()
  }, [idOrder, token])
  return (
    mounted && (
      <form
        onSubmit={e => {
          e.preventDefault()
          const data = {
            id_order: idOrder,
            order_cargo_num_peace: weightSlots2,
            order_cargo_type_pack: cargoPack,
            order_cargo_name: document.getElementById('package_name').value,
            order_cargo_volume:
              parseFloat(
                (
                  document.getElementById('cargo_size_total_gen') ||
                  document.getElementById('cargo_size_total_calc')
                ).value
              ) || 0,
            order_cargo_weight: document.getElementById(
              'cargo_weight_total_calc'
            )
              ? parseFloat(
                  document.getElementById('cargo_weight_total_calc').value
                ) || 0
              : (parseFloat(
                  document.getElementById('cargo_weight_total').value
                ) || 0) * weightSlots2,
            order_cargo_count_peace: true,
          }
          data.order_cargo_peace_data = [...Array(weightSlots2).keys()].map(
            k => {
              return {
                number_peace: k,
                size:
                  document.getElementById(`cargo_size_${k}_l`).value +
                  'x' +
                  document.getElementById(`cargo_size_${k}_w`).value +
                  'x' +
                  document.getElementById(`cargo_size_${k}_h`).value,
                weight:
                  parseFloat(
                    document.getElementById(`cargo_weight_${k}`).value
                  ) || 0,
              }
            }
          )

          if (data.order_cargo_volume && data.order_cargo_weight) {
            axios
              .put(STEP3, data, {
                headers: { token: token },
              })
              .then(() => {
                setProgress(3)
                if (initialProgress < 3) {
                  setInitialProgress(3)
                }
                scrollToTargetAdjusted('tab4')
              })
          } else {
            setError('Проверьте заполнение полей')
          }
        }}
      >
        <>
          <FormField
            label="Кол-во мест груза, шт."
            required={true}
            name="weight"
          >
            <input
              type="number"
              step="1"
              name="weightSlots"
              id="weightSlots"
              min="1"
              max="50"
              required
              className="form-input"
              value={weightSlots}
              onChange={e => {
                setWeightSlots(parseInt(e.target.value))
              }}
              onBlur={() => {
                let val
                if (!weightSlots || weightSlots <= 1) {
                  val = 1
                } else if (weightSlots >= 50) {
                  val = 50
                } else {
                  val = weightSlots
                }
                setWeightSlots(val)
                setWeightSlots2(val)
                calcStep3()
              }}
            />
          </FormField>
          <FormField label="Вид упаковки" required={false}>
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
              styles={fieldSelectStyle}
              name="cargo_pack"
              required
              id="cargo_pack"
              isDisabled={false}
              defaultValue={cargoPack}
              value={cargoPack}
              onChange={e => {
                console.log(e)
                setCargoPack(Object.assign({}, e))
                console.log(cargoPack)
              }}
              options={cargoPacks}
            />
            <FontAwesomeIcon
              icon={faAngleDown}
              className={'input-angle'}
              style={{
                position: 'absolute',
                top: '12px',
                right: '30px',
              }}
              size="lg"
            />
          </FormField>
          <FormField label="Наименование груза">
            <input
              type="text"
              className="form-input"
              placeholder={'Запчасти'}
              name={'package_name'}
              id={'package_name'}
            />
          </FormField>
          {[...Array(weightSlots2).keys()].map((k, i) => {
            return (
              <>
                <FormField
                  label={<>Габариты и вес места №{k + 1}</>}
                  key={k + 1}
                  required={true}
                  style={{ display: rowsFilled[k] ? 'flex' : 'none' }}
                >
                  <div className="row">
                    <div className="col-3 col-3-param">
                      <input
                        type="number"
                        min="0.000001"
                        step="any"
                        required
                        className="form-input"
                        placeholder={'100'}
                        name={`cargo_size_${k}_l`}
                        id={`cargo_size_${k}_l`}
                        onFocus={() => setError('')}
                        onBlur={calcStep3}
                        onInput={e => {
                          checkRow(k)
                        }}
                      />
                    </div>
                    <div className="col-3 col-3-param">
                      <input
                        type="number"
                        min="0.000001"
                        step="any"
                        required
                        className="form-input"
                        placeholder={'50'}
                        name={`cargo_size_${k}_w`}
                        id={`cargo_size_${k}_w`}
                        onFocus={() => setError('')}
                        onBlur={calcStep3}
                        onInput={() => {
                          checkRow(k)
                        }}
                      />
                    </div>
                    <div className="col-3 col-3-param">
                      <input
                        type="number"
                        min="0.000001"
                        step="any"
                        required
                        className="form-input"
                        placeholder={'50'}
                        name={`cargo_size_${k}_h`}
                        id={`cargo_size_${k}_h`}
                        onFocus={() => setError('')}
                        onBlur={calcStep3}
                        onInput={() => {
                          checkRow(k)
                        }}
                      />
                    </div>
                    <div className="col-3 col-3-param">
                      <input
                        type="number"
                        min="0.000001"
                        step="any"
                        required
                        className="form-input"
                        placeholder={'80'}
                        name={`cargo_weight_${k}`}
                        id={`cargo_weight_${k}`}
                        onFocus={() => setError('')}
                        onBlur={e => calcStep3()}
                        onInput={e => {
                          checkRow(k)
                        }}
                      />
                    </div>
                  </div>
                </FormField>
              </>
            )
          })}

          {
            <>
              <FormField label="">
                <div className="row tip">
                  <div className="col-3 col-3-param orange text-center">
                    <p>Длина, см</p>
                  </div>
                  <div className="col-3 col-3-param orange text-center">
                    <p>Ширина, см</p>
                  </div>
                  <div className="col-3 col-3-param orange text-center">
                    <p>Высота, см</p>
                  </div>
                  <div className="col-3 col-3-param orange text-center">
                    <p>Вес, кг</p>
                  </div>
                </div>
              </FormField>
            </>
          }
          <FormField label="Расчётные параметры груза" required={true}>
            <div className="row">
              <div className="col-3 col-3-param">
                <input
                  type="number"
                  required
                  className="form-input"
                  placeholder={''}
                  disabled
                  name={`cargo_size_total_calc`}
                  id={`cargo_size_total_calc`}
                />
              </div>
              <div className="col-3 col-3-param">
                <input
                  type="number"
                  required
                  disabled
                  className="form-input"
                  value={weight}
                  placeholder={''}
                />
              </div>
              <div className="col-3 col-3-param">
                <input
                  type="number"
                  required
                  disabled
                  className="form-input"
                  placeholder={''}
                  name={`cargo_weight_total_calc`}
                  id={`cargo_weight_total_calc`}
                />
              </div>
              <div className="col-3 col-3-param">
                <input
                  type="number"
                  required
                  disabled
                  className="form-input"
                  name={`cargo_weight_volumed_calc`}
                  id={`cargo_weight_volumed_calc`}
                  placeholder={''}
                />
              </div>
            </div>
          </FormField>
          <FormField label="">
            <div className="row tip">
              <div className="col-3 col-3-param orange text-center">
                <p>Объём, м3</p>
              </div>
              <div className="col-3 col-3-param orange text-center">
                <p>Заявленный вес, кг</p>
              </div>
              <div className="col-3 col-3-param orange text-center">
                <p>Расчётный вес, кг</p>
              </div>
              <div className="col-3 col-3-param orange text-center">
                <p>Объёмный вес, кг</p>
              </div>
            </div>
          </FormField>
          <FormField label="Платный вес груза, кг" required={true}>
            <input
              type="number"
              step="any"
              min="0"
              required
              disabled={true}
              className="form-input"
              placeholder={''}
              name={`cargo_payed`}
              id={`cargo_payed`}
            />
          </FormField>
          <FormField label="">
            <p className={'orange mt-0'}>
              Проверьте введённые поля и для продолжения подтвердите данные
            </p>
          </FormField>
          {error && (
            <FormField label="">
              <p className={'white'}>{error}</p>
            </FormField>
          )}
          <FormField>
            <button
              type="submit"
              className="btn calc-btn mt-0"
              style={{
                width: '100%',
              }}
            >
              Подтвердить
            </button>
          </FormField>
        </>
      </form>
    )
  )
}
export default Step3
