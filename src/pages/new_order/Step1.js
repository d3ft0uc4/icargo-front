import axios from 'axios'
import { STEP1 } from '../../lib/backendRoutes'
import FormField from '../../components/FormField'
import React, { useState, useEffect } from 'react'
import getOrder from '../../utils/getOrder'
import { findByRateAirline, scrollToTargetAdjusted } from '../../utils/misc'
import Select from 'react-select'
import fieldSelectStyle from '../../utils/fieldSelectStyle'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/fontawesome-free-solid'
import orderBy from 'lodash/orderBy'

const getInitialRates = state => {
  if (state && state.data && state.data.rates) {
    const r = orderBy(state.data.rates, 'rate', 'asc')
    const rates = r.map(item => ({
      label: `${item.rate} (а/к ${item.airline.airline_name})`,
      value: item.id_rate,
      rate: item.rate,
      airline: item.airline.airline_name,
    }))
    const defaultRate = rates[0]
    return { d: defaultRate, r: rates }
  }
  return { d: {}, r: [] }
}

const Step1 = ({
  token,
  setProgress,
  idOrder,
  state,
  initialProgress,
  setInitialProgress,
}) => {
  const [mounted, setMounted] = useState(false)
  const [weight, setWeight] = useState(0)
  const [order, setOrder] = useState({})
  const rates = getInitialRates(state).r
  const [rate, setRate] = useState(getInitialRates(state).d)
  const [passed, setPassed] = useState(false)
  const [rateLabel, setRateLabel] = useState('')
  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        setMounted(true)
        setOrder(data)
        setWeight(data.cargo_weight)
        if (data.rate) {
          setPassed(true)
          setRateLabel(`${data.rate} (а/к ${data.airline_name})`)
        }
      })
      .catch()
  }, [idOrder, token])
  return (
    mounted && (
      <form
        onSubmit={e => {
          e.preventDefault()
          axios
            .put(STEP1, {
              id_order: idOrder,
              id_rate: rate.value,
              id_type_cargo: 100,
              weight: weight,
            })
            .then(() => {
              setProgress(1)
              if (initialProgress < 1) {
                setInitialProgress(1)
              }
              scrollToTargetAdjusted('tab2')
            })
            .catch(() => {})
        }}
      >
        <>
          <FormField label="Пункт вылета" required={true}>
            <>
              <input
                type="text"
                className="form-input"
                required
                value={
                  state.valueFrom ||
                  `${order.city_name_from || ''}, ${order.name_airport_from ||
                    ''}`
                }
                name="from"
                disabled={true}
              />
              <p
                style={{
                  position: 'absolute',
                  color: 'black',
                  right: '30px',
                  top: '11px',
                }}
              >
                <span
                  className="airport-code"
                  style={{ color: 'rgb(84,84,84)' }}
                >
                  {order.code_iata_from || state.codeFrom}
                </span>
              </p>
            </>
          </FormField>
          <FormField label="Пункт назначения" required={true}>
            <input
              type="text"
              className="form-input"
              required
              value={
                state.valueTo ||
                `${order.city_name_to || ''}, ${order.name_airport_to || ''}`
              }
              name="from"
              disabled={true}
            />
            <p
              style={{
                position: 'absolute',
                color: 'black',
                right: '30px',
                top: '11px',
              }}
            >
              <span className="airport-code" style={{ color: 'rgb(84,84,84)' }}>
                {order.code_iata_to || state.codeTo}
              </span>
            </p>
          </FormField>
          <FormField label="Тип груза" required={true}>
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
              styles={fieldSelectStyle}
              name="cargo_type"
              required
              id="cargo_type"
              isDisabled={true}
              defaultValue={{
                label: 'Обычный груз',
                value: '1',
              }}
              options={[
                {
                  label: 'Обычный груз',
                  value: '1',
                },
              ]}
            />
          </FormField>
          <FormField label="Вес партии, кг." required={true} name="weight">
            <input
              disabled={true}
              type="number"
              step="any"
              name="weight"
              min="0.01"
              required
              className="form-input"
              value={weight}
            />
          </FormField>
          <FormField label="Тариф, руб/кг" required={true}>
            {!passed && (
              <>
                <Select
                  components={{
                    DropdownIndicator: () => null,
                    IndicatorSeparator: () => null,
                  }}
                  isSearchable={false}
                  styles={fieldSelectStyle}
                  name="cargo_type"
                  isRequired={true}
                  id="id_rate"
                  isDisabled={passed}
                  defaultValue={rate}
                  onChange={e => setRate(e)}
                  options={rates}
                />
                <FontAwesomeIcon
                  icon={faAngleDown}
                  className={'input-angle'}
                  style={{
                    position: 'absolute',
                    top: '12px',
                    right: '30px',
                  }}
                  size="lg"
                />
              </>
            )}
            {passed && (
              <input
                disabled={true}
                type="text"
                className="form-input"
                value={rateLabel}
              />
            )}
          </FormField>
          {!passed && (
            <>
              {' '}
              <FormField label="">
                <p className={'orange mt-0'}>
                  Выберите подходящее условие отправки груза и для продолжения
                  подтвердите данные
                </p>
              </FormField>
              <FormField>
                <button
                  type="submit"
                  className="btn calc-btn mt-0"
                  style={{
                    width: '100%',
                  }}
                >
                  Подтвердить
                </button>
              </FormField>
            </>
          )}
        </>
      </form>
    )
  )
}
export default Step1
