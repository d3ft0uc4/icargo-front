import axios from 'axios'
import { ACCEPT} from '../../lib/backendRoutes'
import React, { useEffect, useState } from 'react'
import toFixed from '../../utils/toFixed'
import getOrder from '../../utils/getOrder'

const StepAccept = props => {
  const [mounted, setMounted] = useState(false)
  const { setLoading, idOrder, token, setSuccessOrder } = props
  const [tariff, setTariff] = useState('')
  const [valueByRate, setValueByRate] = useState('')
  const [valueByServices, setValueByServices] = useState('')
  const [valueSale, setValueSale] = useState('')
  const [valueTotal, setValueTotal] = useState('')
  const [payedWeight, setPayedWeight] = useState('')
  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        setTariff(toFixed(data.rate))
        setValueTotal(toFixed(data.value_total))
        setValueByRate(toFixed(data.value_by_rate))
        setValueByServices(toFixed(data.value_by_services))
        setValueSale(toFixed(data.value_sale))
        setPayedWeight(toFixed(data.weight_paid))
        setMounted(true)
      })
      .catch()
  }, [idOrder, token])
  return (
    mounted && (
      <>
        <div className="mt-4 border-top">
          <div className="row">
            <div
              className="col-lg-12 mt-3"
              style={{ fontWeight: '100 !important' }}
            >
              <h5 style={{ float: 'left' }}>
                Стоимость отправки груза, в т.ч.
              </h5>
              <h5 style={{ float: 'right' }}>
                <span className="orange">{valueTotal}</span> {' руб.'}
              </h5>
            </div>
            <div className="col-lg-12 mt-1 fa-w-1">
              <h6 style={{ fontWeight: 100, float: 'left' }}>
                по тарифу{' '}
                <span className="orange">
                  {parseFloat(tariff).toFixed(2)} руб./кг
                </span>{' '}
                на авиаперевозку{' '}
                <span className="orange">{payedWeight} кг </span>
                платного веса
              </h6>
              <h6 style={{ fontWeight: 100, float: 'right' }}>
                {valueByRate} {' руб.'}
              </h6>
            </div>
            <div className="col-lg-12 mt-1">
              <h6 style={{ fontWeight: 100, float: 'left' }}>
                дополнительные сборы
              </h6>
              <h6 style={{ fontWeight: 100, float: 'right' }}>
                {valueByServices} {' руб.'}
              </h6>
            </div>
            <div className="col-lg-12 mt-1">
              <h6 style={{ fontWeight: 100, float: 'left' }}>
                скидка (оформление заявки на сайте cargobooking.ru)
              </h6>
              <h6 style={{ fontWeight: 100, float: 'right' }}>
                {valueSale} {' руб.'}
              </h6>
            </div>
          </div>
        </div>
        <div className="mt-1 border-top">
          <div className="row mt-3 mb-2">
            <div className="col-lg-6" style={{ fontWeight: '100 !important' }}>
              <p className="text-justify">
                Нажимая кнопку оформить заказ Вы подтверждаете своё согласие с
                условиями{' '}
                <span style={{ color: '#FF9600', borderBottom: '1px solid' }}>
                  Лицензионного соглашения
                </span>{' '}
                и{' '}
                <span style={{ color: '#FF9600', borderBottom: '1px solid' }}>
                  Оферты
                </span>{' '}
              </p>
            </div>
            <div className="col-lg-6" style={{ fontWeight: '100 !important' }}>
              <button
                type="button"
                className="btn calc-btn mt-0 w-100"
                onClick={e => {
                  e.preventDefault()
                  setLoading(true)
                  axios
                    .put(
                      ACCEPT,
                      {
                        id_order: idOrder,
                      },
                      { headers: { token: token } }
                    )
                    .then(() => {
                      setSuccessOrder(true)
                    })
                    .catch(() => {})
                    .finally(() => {
                      setLoading(false)
                    })
                }}
              >
                Оформить заказ
              </button>
            </div>
          </div>
        </div>
      </>
    )
  )
}
export default StepAccept
