/* eslint-disable react-hooks/exhaustive-deps */
import axios from 'axios'
import {
  CHECK_INN,
  CHECK_PASSPORT,
  CHECK_PHONE,
  GET_CONTACT_ORDER,
  LOGIN,
  STEP2,
} from '../../lib/backendRoutes'
import FormField from '../../components/FormField'
import React, { useState, useEffect } from 'react'
import getOrder from '../../utils/getOrder'
import {
  scrollToTargetAdjusted,
  stripNonDigits,
  unwrapINN,
  unwrapKPP,
  validateEmail,
} from '../../utils/misc'
import Select from 'react-select'
import fieldSelectStyle from '../../utils/fieldSelectStyle'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/fontawesome-free-solid'
import InputMask from 'react-input-mask'
import { HashLink as Link } from 'react-router-hash-link'
import Cookies from 'universal-cookie'
import useGlobal from '../../lib/hookstore'

const defaultClient = {
  label: 'Новый клиент',
  value: 'new',
  id_type_client: 'UL',
}

const Step2 = ({
  token,
  idContact,
  setProgress,
  idOrder,
  initialProgress,
  setInitialProgress,
}) => {
  const cookies = new Cookies()
  const globalActions = useGlobal()[1]
  const [error, setError] = useState('')

  const [mounted, setMounted] = useState(false)
  const [licAgreed, setLicAgreed] = useState(false)
  const [clients, setClients] = useState([defaultClient])

  const [inputSMS, setInputSMS] = useState('')
  const [showCodeSMS, setShowCodeSMS] = useState(false)
  const [SMSPassed, setSMSPassed] = useState(!!(idContact && token))
  const [inputFIOStep2, setInputFIOStep2] = useState('')
  const [inputEmailStep2, setInputEmailStep2] = useState('')
  const [inputClientTypeStep2, setinputClientTypeStep2] = useState('')
  const [inputPhone, setInputPhone] = useState(
    idContact && token ? idContact : ''
  )

  const [INNStep2, setINNStep2] = useState('')
  const [KPPStep2, setKPPStep2] = useState('')
  const [companyNameStep2, setCompanyNameStep2] = useState('')
  const [companyAddressStep2, setCompanyAddressStep2] = useState('')

  const [passportStep2, setPassportStep2] = useState('')
  const [FIO2Step2, setFIO2Step2] = useState('')
  const [addressStep2, setAddressStep2] = useState('')
  const [contactPhoneStep2, setContactPhoneStep2] = useState('')

  const setClient = client => {
    setinputClientTypeStep2(client.id_type_client)
    if (client.id_type_client === 'UL') {
      setINNStep2((client.id_client || '').toString())
      setKPPStep2((client.kpp || '').toString())
      setCompanyAddressStep2((client.address || '').toString())
      setContactPhoneStep2((client.id_client_phone || '').toString())
      setCompanyNameStep2((client.name_client || '').toString())
    } else {
      setPassportStep2((client.id_client || '').toString())
      setAddressStep2((client.address || '').toString())
      setFIO2Step2((client.name_client || '').toString())
      setAddressStep2((client.address || '').toString())
      setContactPhoneStep2((client.id_client_phone || '').toString())
    }
  }

  const clearClient = () => {
    setINNStep2('')
    setKPPStep2('')
    setCompanyAddressStep2('')
    setCompanyNameStep2('')
    setPassportStep2('')
    setAddressStep2('')
    setFIO2Step2('')
    setAddressStep2('')
    setContactPhoneStep2('')
  }

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const getContactOrder = () => {
    const cookies = new Cookies()
    const token = cookies.get('token')
    const idContact = cookies.get('id_contact')
    if (!token || !idContact) return
    axios
      .post(
        GET_CONTACT_ORDER,
        {
          phone_number: parseInt(idContact),
        },
        {
          headers: { token: token },
        }
      )
      .then(response => {
        if (response.data.status_contact === 1) {
          const d = response.data.data
          const clients = d.data_client.map(item => {
            return {
              label: `${item.name_client}`,
              value: item.id_client,
              ...item,
            }
          })
          setClients([defaultClient, ...clients])
          setInputEmailStep2(d.email_contact)
          setInputFIOStep2(d.name_contact)
        }
      })
  }
  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        setClient({
          id_type_client: data.customer_type,
          id_client: data.customer_id,
          name_client: data.customer_name,
          address: data.customer_address,
          id_client_phone: data.customer_phone,
          kpp: data.customer_kpp,
        })
      })
      .then(getContactOrder)
      .then(() => setMounted(true))
      .catch()
  }, [idOrder, token, SMSPassed])
  return (
    mounted && (
      <form
        onSubmit={e => {
          e.preventDefault()
          const formValid =
            idOrder &&
            inputFIOStep2 &&
            inputEmailStep2 &&
            inputClientTypeStep2 &&
            (inputClientTypeStep2 === 'UL'
              ? unwrapINN(INNStep2)
              : passportStep2) &&
            (inputClientTypeStep2 === 'UL'
              ? unwrapKPP(KPPStep2) || unwrapINN(INNStep2).length === 12
              : true) &&
            (inputClientTypeStep2 === 'UL' ? companyNameStep2 : FIO2Step2) &&
            (inputClientTypeStep2 === 'UL'
              ? companyAddressStep2
              : addressStep2) &&
            contactPhoneStep2
          if (!formValid) {
            setError('Заполните обязательные поля')
          } else {
            const cookies = new Cookies()
            const token = cookies.get('token')
            axios
              .put(
                STEP2,
                {
                  id_order: idOrder,
                  name_contact: inputFIOStep2,
                  id_contact: stripNonDigits(inputPhone),
                  email_contact: inputEmailStep2,
                  type_client: inputClientTypeStep2,
                  data_client: {
                    id_client: stripNonDigits(
                      inputClientTypeStep2 === 'UL'
                        ? unwrapINN(INNStep2)
                        : passportStep2
                    ),
                    kpp:
                      inputClientTypeStep2 === 'UL'
                        ? unwrapKPP(KPPStep2)
                        : null,
                    name:
                      inputClientTypeStep2 === 'UL'
                        ? companyNameStep2
                        : FIO2Step2,
                    address:
                      inputClientTypeStep2 === 'UL'
                        ? companyAddressStep2
                        : addressStep2,
                    phone_client: stripNonDigits(contactPhoneStep2),
                  },
                },
                { headers: { token: token } }
              )
              .then(() => {
                setProgress(2)
                if (initialProgress < 2) {
                  setInitialProgress(2)
                }
                scrollToTargetAdjusted('tab3')
              })
          }
        }}
      >
        <FormField label="Контактный телефон" required={true}>
          <InputMask
            className="form-input"
            placeholder="+7 (999) 999-99-99"
            mask="+7 (999) 999-99-99"
            maskChar={null}
            id="input-phone"
            alwaysShowMask={true}
            value={inputPhone}
            onInput={e => setInputPhone(e.target.value)}
            disabled={SMSPassed}
          />
        </FormField>
        <FormField
          label=""
          style={{
            display: !showCodeSMS && !SMSPassed ? 'flex' : 'none',
          }}
        >
          <div className="checkbox-wrapper">
            <label className="control control-checkbox">
              Подтверждая номер телефона, вы соглашаетесь с{' '}
              <Link
                className="text-white"
                to="/lic"
                style={{ textDecoration: 'underline' }}
              >
                Лицензионным соглашением и Соглашением об обработке персональных
                данных
              </Link>
              <input
                type="checkbox"
                value={licAgreed}
                onChange={e => setLicAgreed(!licAgreed)}
              />
              <div className="control_indicator" />
            </label>
          </div>
        </FormField>
        <FormField
          label=""
          style={{
            display:
              inputPhone.replace(/\D/g, '').slice(0, 11).length === 11 &&
              !SMSPassed &&
              licAgreed
                ? 'block'
                : 'none',
          }}
        >
          <button
            type="button"
            className="btn calc-btn mt-0"
            onClick={e => {
              e.preventDefault()
              setLicAgreed(false)
              const inpPhone = inputPhone.replace(/\D/g, '').slice(0, 11)
              if (inpPhone.length !== 11) {
                setError('Неправильный номер телефона')
              } else {
                axios.post(CHECK_PHONE, { phone_number: inpPhone }).then(d => {
                  if (d.data.status_code === 100) {
                    setError('СМС отправлено на указанный номер телефона')
                    setShowCodeSMS(true)
                  }
                })
              }
            }}
            style={{
              width: '100%',
            }}
          >
            Подтвердить
          </button>
        </FormField>
        <FormField
          label="Код из СМС"
          required={true}
          style={{
            display: showCodeSMS ? 'flex' : 'none',
          }}
        >
          <InputMask
            className="form-input"
            placeholder="0123"
            mask="9999"
            maskChar={null}
            value={inputSMS}
            onInput={e => {
              setInputSMS(e.target.value)
              const inpPhone = inputPhone.replace(/\D/g, '').slice(0, 11)
              const inp = e.target.value.replace(/\D/g, '').slice(0, 4)
              if (inpPhone.length === 11 && inp.length === 4) {
                axios
                  .post(LOGIN, {
                    id_contact: parseInt(inpPhone),
                    code: parseInt(inp),
                  })
                  .then(response => {
                    cookies.set('token', response.data.token, {
                      path: '/',
                    })
                    cookies.set(
                      'id_contact',
                      parseInt(inputPhone.replace(/\D/g, '').slice(0, 11)),
                      {
                        path: '/',
                      }
                    )
                    globalActions.setPhone(
                      parseInt(inputPhone.replace(/\D/g, '').slice(0, 11))
                    )
                    setError('')
                    setSMSPassed(true)
                    setShowCodeSMS(false)
                  })
                  .then(getContactOrder)
                  .catch(d => {
                    setError('Неправильный код СМС')
                  })
              }
            }}
            disabled={SMSPassed}
          />
        </FormField>
        <FormField
          label="Контактное лицо, ФИО"
          required={true}
          style={{
            display: SMSPassed ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            className="form-input"
            name="fio"
            placeholder={'Иванов Иван Иванович'}
            value={inputFIOStep2}
            onChange={e => setInputFIOStep2(e.target.value)}
            onInput={e => setInputFIOStep2(e.target.value)}
          />
        </FormField>
        <FormField
          label="Контактный email"
          required={true}
          style={{
            display: SMSPassed ? 'flex' : 'none',
          }}
        >
          <input
            type="email"
            className="form-input"
            name="email"
            placeholder={'email@example.com'}
            value={inputEmailStep2}
            onChange={e => setInputEmailStep2(e.target.value)}
            onInput={e => setInputEmailStep2(e.target.value)}
          />
        </FormField>
        <FormField
          label="Выберите клиента"
          required={true}
          style={{
            display: validateEmail(inputEmailStep2) ? 'flex' : 'none',
          }}
        >
          <Select
            components={{
              DropdownIndicator: () => null,
              IndicatorSeparator: () => null,
            }}
            isSearchable={false}
            styles={fieldSelectStyle}
            name="client_type"
            required
            id="client_type"
            isDisabled={false}
            defaultValue={defaultClient}
            options={clients}
            onChange={e => setClient(e)}
          />
          <FontAwesomeIcon
            icon={faAngleDown}
            className={'input-angle'}
            style={{
              position: 'absolute',
              top: '12px',
              right: '30px',
            }}
            size="lg"
          />
        </FormField>
        <FormField
          label="Тип клиента"
          required={true}
          style={{
            display: validateEmail(inputEmailStep2) ? 'flex' : 'none',
          }}
        >
          <div className="radio-button-group">
            <label>
              <input
                type="radio"
                value={'FL'}
                name={'client_type'}
                checked={inputClientTypeStep2 === 'FL'}
                onChange={e => {
                  clearClient()
                  setinputClientTypeStep2('FL')
                }}
              />
              <span>{'Физ. лицо'}</span>
            </label>
            <label>
              <input
                type="radio"
                value={'UL'}
                name={'client_type'}
                checked={inputClientTypeStep2 === 'UL'}
                onChange={e => {
                  clearClient()
                  setinputClientTypeStep2('UL')
                }}
              />
              <span>{'Юр. лицо'}</span>
            </label>
          </div>
        </FormField>
        <FormField
          label="ИНН"
          required={true}
          style={{
            display:
              validateEmail(inputEmailStep2) && inputClientTypeStep2 === 'UL'
                ? 'flex'
                : 'none',
          }}
        >
          <InputMask
            className="form-input"
            placeholder="999999999999"
            mask="999999999999"
            maskChar={null}
            value={INNStep2}
            onFocus={e => setError('')}
            onInput={e => {
              setINNStep2(e.target.value)
              const inp = e.target.value.replace(/\D/g, '').slice(0, 12)
              if (inp.length === 10 || inp.length === 12) {
                axios
                  .post(CHECK_INN, {
                    id_order: idOrder,
                    inn: inp,
                  })
                  .then(d => {
                    if (d.data.status === 1) {
                      const client = d.data.data
                      setINNStep2((client.id_client || '').toString())
                      setKPPStep2((client.kpp || '').toString())
                      setCompanyAddressStep2(client.address)
                      setContactPhoneStep2(
                        (client.phone_client || '').toString()
                      )
                      setCompanyNameStep2(client.name)
                    } else {
                      setError('Ничего не найдено')
                    }
                  })
              }
            }}
          />
        </FormField>
        <FormField
          label="КПП"
          required={true}
          style={{
            display:
              validateEmail(inputEmailStep2) && inputClientTypeStep2 === 'UL'
                ? 'flex'
                : 'none',
          }}
        >
          <InputMask
            className="form-input"
            placeholder="999999999"
            mask="999999999"
            value={KPPStep2}
            maskChar={null}
            disabled={true}
            onInput={e => setKPPStep2(e.target.value)}
          />
        </FormField>
        <FormField
          label="Наименование компании"
          required={true}
          style={{
            display:
              validateEmail(inputEmailStep2) && inputClientTypeStep2 === 'UL'
                ? 'flex'
                : 'none',
          }}
        >
          <input
            type="text"
            disabled={true}
            className="form-input"
            placeholder={'OOO "КаргоБукинг"'}
            value={companyNameStep2}
            onInput={e => setCompanyNameStep2(e.target.value)}
          />
        </FormField>
        <FormField
          label="Юр. адрес"
          required={true}
          style={{
            display:
              validateEmail(inputEmailStep2) && inputClientTypeStep2 === 'UL'
                ? 'flex'
                : 'none',
          }}
        >
          <input
            type="text"
            disabled={true}
            className="form-input"
            placeholder={'141700, г. Химки, ш. Международное, вл. 1'}
            value={companyAddressStep2}
            onInput={e => setCompanyAddressStep2(e.target.value)}
          />
        </FormField>
        <FormField
          label="Номер паспорта"
          required={true}
          style={{
            display:
              validateEmail(inputEmailStep2) && inputClientTypeStep2 === 'FL'
                ? 'flex'
                : 'none',
          }}
        >
          <InputMask
            className="form-input"
            placeholder="9999 999999"
            mask="9999 999999"
            value={passportStep2}
            maskChar={null}
            onFocus={e => setError('')}
            onInput={e => {
              setPassportStep2(e.target.value)
              const inp = e.target.value.replace(/\D/g, '').slice(0, 10)
              if (inp.length === 10) {
                axios
                  .post(CHECK_PASSPORT, {
                    id_order: idOrder,
                    passport: inp,
                  })
                  .then(d => {
                    if (d.data.status === 1) {
                      const client = d.data.data
                      setPassportStep2(client.id_client)
                      setAddressStep2(client.address)
                      setContactPhoneStep2(
                        (client.phone_client || '')
                          .toString()
                          .replace(/\D/g, '')
                      )
                      setFIO2Step2(client.name)
                    } else {
                      setError('Ничего не найдено')
                    }
                  })
              }
            }}
          />
        </FormField>
        <FormField
          label="ФИО"
          required={true}
          style={{
            display:
              validateEmail(inputEmailStep2) && inputClientTypeStep2 === 'FL'
                ? 'flex'
                : 'none',
          }}
        >
          <input
            type="text"
            className="form-input"
            placeholder={'Иванов Иван Иванович'}
            value={FIO2Step2}
            onChange={e => setFIO2Step2(e.target.value)}
          />
        </FormField>
        <FormField
          label="Адрес проживания"
          required={true}
          style={{
            display:
              validateEmail(inputEmailStep2) && inputClientTypeStep2 === 'FL'
                ? 'flex'
                : 'none',
          }}
        >
          <input
            type="text"
            className="form-input"
            placeholder={'МО, г Химки, ш Международное, вл 1'}
            value={addressStep2}
            onChange={e => setAddressStep2(e.target.value)}
          />
        </FormField>
        <FormField
          label="Телефон"
          required={true}
          style={{
            display:
              validateEmail(inputEmailStep2) && inputClientTypeStep2
                ? 'flex'
                : 'none',
          }}
        >
          <InputMask
            className="form-input"
            placeholder="+7 (999) 999-99-99"
            mask="+7 (999) 999-99-99"
            id="input-phone"
            maskChar={null}
            value={contactPhoneStep2}
            onInput={e => setContactPhoneStep2(e.target.value)}
          />
        </FormField>
        {error && (
          <FormField label="">
            <p className={'white'}>{error}</p>
          </FormField>
        )}
        <FormField label="">
          <p className={'orange mt-0'}>
            Проверьте введённые поля и для продолжения подтвердите данные
          </p>
        </FormField>
        <FormField>
          {SMSPassed && (
            <button
              type="submit"
              className="btn calc-btn mt-0"
              style={{
                width: '100%',
              }}
            >
              Подтвердить
            </button>
          )}
        </FormField>
      </form>
    )
  )
}
export default Step2
