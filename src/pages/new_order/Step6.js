import axios from 'axios'
import {
  CHECK_INN,
  CHECK_PASSPORT,
  STEP6,
  STEP7,
} from '../../lib/backendRoutes'
import FormField from '../../components/FormField'
import React, { useState, useEffect } from 'react'
import { scrollToTargetAdjusted, unwrapINN } from '../../utils/misc'
import InputMask from 'react-input-mask'
import getOrder from '../../utils/getOrder'
import toFixed from '../../utils/toFixed'

const Step6 = ({
  token,
  setProgress,
  idOrder,
  progress,
  setInitialProgress,
  initialProgress,
}) => {
  const stepFieldHelper = (flag, o = undefined) => {
    const ord = order || o
    if (flag) {
      setCheckFlag(true)
      setInputClientType(ord.sender_type)
      setINN(ord.sender_id)
      setCompanyName(ord.sender_name)
      setCompanyAddress(ord.sender_address)
      setPassport(ord.sender_id)
      setFIO(ord.sender_name)
      setAddress(ord.sender_address)
      setContactPhone(ord.sender_phone)
    } else {
      setInputClientType('')
      setINN('')
      setCompanyName('')
      setCompanyAddress('')
      setPassport('')
      setFIO('')
      setAddress('')
      setContactPhone('')
    }
  }
  const [same, setSame] = useState(undefined)
  const [inputClientType, setInputClientType] = useState('')
  const [INN, setINN] = useState('')
  const [checkFlag, setCheckFlag] = useState(progress < initialProgress)
  const [companyName, setCompanyName] = useState('')
  const [companyAddress, setCompanyAddress] = useState('')

  const [passport, setPassport] = useState('')
  const [FIO, setFIO] = useState('')
  const [address, setAddress] = useState('')
  const [contactPhone, setContactPhone] = useState('')

  const [error, setError] = useState('')
  const [order, setOrder] = useState({})
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        setMounted(true)
        if (initialProgress >= 6) {
          if (data.sender_id === data.recipient_id && data.sender_id) {
            setSame(true)
            setInputClientType(data.sender_type)
            setINN((data.sender_id || '').toString())
            setCompanyName(data.sender_name)
            setCompanyAddress(data.sender_address)
            setPassport((data.sender_id || '').toString())
            setFIO(data.sender_name)
            setAddress(data.sender_address)
            setContactPhone(data.sender_phone)
          } else {
            setSame(false)
            setInputClientType(data.recipient_type)
            setINN((data.recipient_id || '').toString())
            setCompanyName(data.recipient_name)
            setCompanyAddress(data.recipient_address)
            setPassport((data.recipient_id || '').toString())
            setFIO(data.recipient_name)
            setAddress(data.recipient_address)
            setContactPhone(data.recipient_phone)
          }
        }
        setOrder(data)
      })
      .catch()
  }, [
    idOrder,
    initialProgress,
    order.sender_address,
    order.sender_id,
    order.sender_name,
    order.sender_phone,
    order.sender_type,
    token,
  ])

  return (
    mounted && (
      <form
        onSubmit={e => {
          e.preventDefault()
          const formValid =
            idOrder &&
            checkFlag &&
            inputClientType &&
            (inputClientType === 'UL' ? unwrapINN(INN) : passport) &&
            (inputClientType === 'UL' ? companyName : FIO) &&
            (inputClientType === 'UL' ? companyAddress : address) &&
            contactPhone
          if (!formValid) {
            setError('Заполните обязательные поля')
          } else {
            axios
              .put(
                STEP6,
                {
                  id_order: idOrder,
                  order_recipient_type: inputClientType,
                  order_recipient_id:
                    inputClientType === 'UL' ? unwrapINN(INN) : passport,
                  order_recipient_name:
                    inputClientType === 'UL' ? companyName : FIO,
                  order_recipient_address:
                    inputClientType === 'UL' ? companyAddress : address,
                  order_recipient_phone: (contactPhone || '').toString(),
                },
                { headers: { token: token } }
              )
              .then(() => {
                setProgress(6)
                if (initialProgress < 6) {
                  setInitialProgress(6)
                }
                scrollToTargetAdjusted('tab7')
              })
          }
        }}
      >
        <FormField
          label={
            <>
              {'Грузополучатель является'} <br />
              {'грузоотправителем?'}
            </>
          }
          lclass="lh-initial"
          required={true}
        >
          <div className="radio-button-group">
            <label>
              <input
                type="radio"
                value={'1'}
                name={'sameAs4'}
                checked={same === true}
                onChange={e => {
                  setSame(true)
                  stepFieldHelper(true)
                  setCheckFlag(true)
                }}
              />
              <span>{'Да'}</span>
            </label>
            <label>
              <input
                type="radio"
                value={'0'}
                name={'sameAs4'}
                checked={same === false}
                onChange={e => {
                  setSame(false)
                  stepFieldHelper(false)
                  setCheckFlag(false)
                }}
              />
              <span>{'Нет'}</span>
            </label>
          </div>
        </FormField>
        <FormField
          label="Тип грузоополучателя"
          required={true}
          style={{
            display: same === false ? 'flex' : 'none',
          }}
        >
          <div className="radio-button-group">
            <label>
              <input
                type="radio"
                value={'FL'}
                name={'client_type'}
                checked={inputClientType === 'FL'}
                onChange={e => {
                  setCheckFlag(false)
                  stepFieldHelper(false)
                  setInputClientType('FL')
                }}
              />
              <span>{'Физ. лицо'}</span>
            </label>
            <label>
              <input
                type="radio"
                value={'UL'}
                name={'client_type'}
                checked={inputClientType === 'UL'}
                onChange={e => {
                  setCheckFlag(false)
                  stepFieldHelper(false)
                  setInputClientType('UL')
                }}
              />
              <span>{'Юр. лицо'}</span>
            </label>
          </div>
        </FormField>
        <FormField
          label="ИНН грузополучателя"
          required={true}
          style={{
            display: inputClientType === 'UL' ? 'flex' : 'none',
          }}
        >
          <InputMask
            className="form-input"
            disabled={same}
            placeholder="999999999999"
            mask="999999999999"
            maskChar={null}
            value={INN}
            onFocus={e => setError('')}
            onInput={e => {
              setINN(e.target.value)
              setCheckFlag(false)
              const inp = e.target.value.replace(/\D/g, '').slice(0, 12)
              if (inp.length === 10 || inp.length === 12) {
                axios
                  .post(CHECK_INN, {
                    id_order: idOrder,
                    inn: inp,
                  })
                  .then(d => {
                    if (d.data.status === 1) {
                      const client = d.data.data
                      setINN(client.id_client)
                      setCompanyAddress(client.address)
                      setContactPhone(client.phone_client)
                      setCompanyName(client.name)
                      setCheckFlag(true)
                    } else {
                      setError('Ничего не найдено')
                    }
                  })
              }
            }}
          />
        </FormField>

        <FormField
          label="Наименование грузополучателя"
          required={true}
          style={{
            display: inputClientType === 'UL' && checkFlag ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            className="form-input"
            disabled={same || true}
            placeholder={'OOO "КаргоБукинг"'}
            value={companyName}
            onInput={e => setCompanyName(e.target.value)}
          />
        </FormField>
        <FormField
          label="Юр. адрес грузополучателя"
          required={true}
          style={{
            display: inputClientType === 'UL' && checkFlag ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            disabled={same || true}
            className="form-input"
            placeholder={'141700, г. Химки, ш. Международное, вл. 1'}
            value={companyAddress}
            onInput={e => setCompanyAddress(e.target.value)}
          />
        </FormField>
        <FormField
          label="Номер паспорта грузополучателя"
          required={true}
          style={{
            display: inputClientType === 'FL' ? 'flex' : 'none',
          }}
        >
          <InputMask
            className="form-input"
            placeholder="____ ______"
            mask="9999 999999"
            disabled={same}
            value={passport}
            maskChar={null}
            onFocus={e => setError('')}
            onInput={e => {
              setPassport(e.target.value)
              setCheckFlag(false)
              const inp = e.target.value.replace(/\D/g, '').slice(0, 10)
              if (inp.length === 10) {
                axios
                  .post(CHECK_PASSPORT, {
                    id_order: idOrder,
                    passport: inp,
                  })
                  .then(d => {
                    if (d.data.status === 1) {
                      const client = d.data.data
                      setPassport(client.id_client)
                      setAddress(client.address)
                      setContactPhone(
                        (client.phone_client || '')
                          .toString()
                          .replace(/\D/g, '')
                      )
                      setFIO(client.name)
                      setCheckFlag(true)
                    } else {
                      setError('Ничего не найдено')
                    }
                  })
              }
            }}
          />
        </FormField>
        <FormField
          label="ФИО грузополучателя"
          required={true}
          style={{
            display: inputClientType === 'FL' && checkFlag ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            disabled={same}
            className="form-input"
            placeholder={'Иванов Иван Иванович'}
            value={FIO}
            onInput={e => setFIO(e.target.value)}
          />
        </FormField>
        <FormField
          label="Адрес проживания грузополучателя"
          required={true}
          style={{
            display: inputClientType === 'FL' && checkFlag ? 'flex' : 'none',
          }}
        >
          <input
            type="text"
            disabled={same}
            className="form-input"
            placeholder={'МО, г Химки, ш Международное, вл 1'}
            value={address}
            onInput={e => setAddress(e.target.value)}
          />
        </FormField>
        <FormField
          label="Телефон грузополучателя"
          required={true}
          style={{
            display: checkFlag ? 'flex' : 'none',
          }}
        >
          <InputMask
            className="form-input"
            placeholder="+7 (999) 999-99-99"
            mask="+7 (999) 999-99-99"
            id="input-phone"
            disabled={same}
            maskChar={null}
            value={contactPhone}
            onInput={e => setContactPhone(e.target.value)}
          />
        </FormField>
        {error && (
          <FormField label="">
            <p className={'white'}>{error}</p>
          </FormField>
        )}
        <FormField label="">
          <p className={'orange mt-0'}>
            Проверьте введённые поля и для продолжения подтвердите данные
          </p>
        </FormField>
        <FormField>
          <button
            type="submit"
            className="btn calc-btn mt-0"
            style={{
              width: '100%',
            }}
          >
            Подтвердить
          </button>
        </FormField>
      </form>
    )
  )
}
export default Step6
