import axios from 'axios'
import { STEP5, STEP7 } from '../../lib/backendRoutes'
import FormField from '../../components/FormField'
import React, { useState, useEffect } from 'react'
import moment from 'moment'
import {
  findByLabel,
  findByValue,
  scrollToTargetAdjusted,
} from '../../utils/misc'
import Select from 'react-select'
import fieldSelectStyle from '../../utils/fieldSelectStyle'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/fontawesome-free-solid'
import getOrder from '../../utils/getOrder'

const dates = [
  new Date(),
  new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
  new Date(new Date().getTime() + 48 * 60 * 60 * 1000),
]

const times = [
  {
    label: 'Утро, 6:00 – 12:00',
    value: '6:00 – 12:00',
  },
  {
    label: 'День, 12:00 – 18:00',
    value: '12:00 – 18:00',
  },
  {
    label: 'Вечер, 18:00 – 22:00',
    value: '18:00 – 22:00',
  },
  {
    label: 'Другое время',
    value: 'other',
  },
]
const words = ['Сегодня', 'Завтра', 'Послезавтра']
const days = ['вс', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб']

const renderDates = dates.map((d, i) => {
  return {
    label: `${words[i]}, ${days[d.getDay()]} ${moment(d).format('DD.MM.YYYY')}`,
    value: `${moment(d).format('DD.MM.YYYY')}`,
  }
})
renderDates[3] = {
  label: 'Другая дата',
  value: 'other',
}

const Step5 = ({
  token,
  setProgress,
  idOrder,
  setInitialProgress,
  initialProgress,
}) => {
  const [mounted, setMounted] = useState(false)
  const [sameWarehouse, setSameWarehouse] = useState(true)
  const [warehouseType, setWarehouseType] = useState({
    label: 'Аэропорт',
    value: '3',
  })
  const [warehouseDate, setWarehouseDate] = useState(renderDates[0])
  const [warehouseTime, setWarehouseTime] = useState(times[0])
  const [otherDate, setOtherDate] = useState('')
  const [otherTime, setOtherTime] = useState('')
  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        if (data.id_state > 6) {
          const d = findByValue(renderDates, data.delivery_date)
          if (d) {
            setWarehouseDate(d)
          } else {
            setWarehouseDate(renderDates[3])
            setOtherDate(data.delivery_date)
          }
          const t = findByValue(times, data.delivery_time)
          if (t) {
            setWarehouseTime(t)
          } else {
            setWarehouseTime(times[3])
            setOtherTime(data.delivery_time)
          }
        }

        setMounted(true)
      })
      .catch()
  }, [idOrder, token])
  return (
    mounted && (
      <form
        onSubmit={e => {
          e.preventDefault()
          axios
            .put(
              STEP5,
              {
                id_order: idOrder,
                order_place_handover: warehouseType,
                order_delivery_address: document.getElementById(
                  'address_accept'
                )
                  ? document.getElementById('address_accept').value
                  : '', // TODO! : addressStep2,
                order_delivery_date:
                  warehouseDate.value === 'other'
                    ? otherDate
                    : warehouseDate.value,
                order_delivery_time:
                  warehouseTime.value === 'other'
                    ? otherTime
                    : warehouseTime.value,
              },
              { headers: { token: token } }
            )
            .then(() => {
              setProgress(5)
              if (initialProgress < 5) {
                setInitialProgress(5)
              }
              scrollToTargetAdjusted('tab6')
            })
        }}
      >
        <>
          <FormField label="Место передачи груза">
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
              styles={fieldSelectStyle}
              name="inp_type"
              required
              id="inp_type"
              isDisabled={true}
              defaultValue={{
                label: 'Аэропорт',
                value: '3',
              }}
              value={warehouseType}
              onChange={e => setWarehouseType(e)}
              options={[
                {
                  label: 'Аэропорт',
                  value: '3',
                },
              ]}
            />
          </FormField>
          <FormField
            lclass="lh-initial"
            label="Адрес склада совпадает с адресом грузоотправителя?"
            hide={warehouseType.value === '3' || warehouseType.value === '2'}
            required={false}
          >
            <div className="radio-button-group">
              <label>
                <input
                  type="radio"
                  checked={sameWarehouse}
                  onChange={e => setSameWarehouse(true)}
                  name={'same_address'}
                />
                <span>{'Да'}</span>
              </label>
              <label>
                <input
                  type="radio"
                  checked={!sameWarehouse}
                  onChange={e => setSameWarehouse(false)}
                  name={'same_address'}
                />
                <span>{'Нет'}</span>
              </label>
            </div>
          </FormField>
          <FormField label="Адрес" required={false}>
            <input
              type="text"
              className="form-input"
              placeholder={'141700, г. Химки, ш. Международное, вл. 1'}
              required={false}
              disabled={true}
              id="address_accept"
            />
          </FormField>
          <FormField label="Плановая дата сдачи груза">
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
              styles={fieldSelectStyle}
              name="date_accept"
              required
              id="date_accept"
              isDisabled={false}
              defaultValue={renderDates[0]}
              value={warehouseDate}
              onChange={e => setWarehouseDate(e)}
              options={renderDates}
            />
            <FontAwesomeIcon
              icon={faAngleDown}
              className={'input-angle'}
              style={{
                position: 'absolute',
                top: '12px',
                right: '30px',
              }}
              size="lg"
            />
          </FormField>
          <FormField label="" hide={warehouseDate.value !== 'other'}>
            <input
              type="text"
              className="form-input"
              id="other_date"
              value={otherDate}
              onChange={e => setOtherDate(e.target.value)}
            />
          </FormField>
          <FormField label="Плановое время сдачи груза">
            <Select
              components={{
                DropdownIndicator: () => null,
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
              styles={fieldSelectStyle}
              name="time_accept"
              required
              id="time_accept"
              isDisabled={false}
              defaultValue={times[0]}
              value={warehouseTime}
              onChange={e => setWarehouseTime(e)}
              options={times}
            />
            <FontAwesomeIcon
              icon={faAngleDown}
              className={'input-angle'}
              style={{
                position: 'absolute',
                top: '12px',
                right: '30px',
              }}
              size="lg"
            />
          </FormField>
          <FormField label="" hide={warehouseTime.value !== 'other'}>
            <input
              type="text"
              className="form-input"
              id="other_time"
              value={otherTime}
              onChange={e => setOtherTime(e.target.value)}
            />
          </FormField>
          <FormField label="">
            <p className={'orange mt-0'}>
              Проверьте введённые поля и для продолжения подтвердите данные
            </p>
          </FormField>
          <FormField>
            <button
              type="submit"
              className="btn calc-btn mt-0"
              style={{
                width: '100%',
              }}
            >
              Подтвердить
            </button>
          </FormField>
        </>
      </form>
    )
  )
}
export default Step5
