import Modal from 'react-modal'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/fontawesome-free-solid'
import React, { useState, useEffect } from 'react'
import getOrder from '../../utils/getOrder'

const customModal = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    background: '#002666',
    border: 'none',
    maxWidth: '600px',
  },
  overlay: {
    background: 'rgba(0,0,0,0.2)',
    zIndex: 1050,
  },
}

const SuccessOrderModal = ({ isOpen, idOrder, token }) => {
  const [date, setDate] = useState('')
  const [email, setEmail] = useState('')
  const [mounted, setMounted] = useState(false)
  useEffect(() => {
    getOrder(token, idOrder)
      .then(data => {
        setDate(data.date_add)
        setEmail(data.contact_email)
        setMounted(true)
      })
      .catch()
  }, [idOrder, isOpen, token])
  return (
    mounted && (
      <Modal isOpen={isOpen} contentLabel="Example Modal" style={customModal}>
        <div
          style={{
            position: 'absolute',
            top: '12px',
            right: '20px',
            zIndex: 1060,
          }}
          onClick={() => (document.location.href = '/')}
        >
          <FontAwesomeIcon
            icon={faTimes}
            style={{ width: '20px', height: '20px' }}
            size="lg"
          />
        </div>

        <div className="row">
          <div className="col-lg-12 text-center">
            <h2 className="mt-2 orange">Заказ оформлен</h2>
            <p className="text text-justify mb-0">
              По Вашему заказу № <span className={'orange'}>{idOrder}</span> от{' '}
              <span className={'orange'}>{date}</span> сформированы документы
              для отправки груза:
            </p>
            <ul className="dashed">
              <li>счёт на предварительную оплату;</li>
              <li>экспедиторское поручение;</li>
              <li>экспедиторская записка.</li>
            </ul>
            <p className="text text-justify mb-0">
              Документы отправлены на указанный при оформлении e-mail:{' '}
              <span className={'orange'}>{email}</span>
              {'. '}
              Проверьте почту и проследуйте дальнейшим инструкциям. Вы также
              можете авторизоваться на нашем сайте по номеру телефона и скачать
              данные документ из личного кабинета.
            </p>
          </div>
        </div>
      </Modal>
    )
  )
}
export default SuccessOrderModal
