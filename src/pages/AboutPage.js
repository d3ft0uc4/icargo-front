import React from 'react'
import Layout from '../components/Layout'

const AboutPage = () => {
  return (
    <Layout>
      <div className="row mt-70">
        <div className="col-lg-12 text-center">
          <h1 className="mt-5">О сервисе</h1>
          <p className="lead">Текст</p>
        </div>
      </div>
    </Layout>
  )
}

export default AboutPage
