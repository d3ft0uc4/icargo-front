import React, { useState } from 'react'
import Layout from '../components/Layout'
import DashboardTab from '../components/DashboardTab'

import Cookies from 'universal-cookie'
import { Redirect } from 'react-router'
import DashboardNavbar from '../components/dashboard/DashboardNavbar'
import Profile from './dashboard/Profile'
import History from './dashboard/History'
import Balance from './dashboard/Balance'

const DashboardPage = () => {
  const cookies = new Cookies()
  const token = cookies.get('token')
  const idContact = cookies.get('id_contact')
  const [active, setActive] = useState('balance')
  const getActive = s => active === s
  if (!token || !idContact) {
    return <Redirect to="/" />
  }
  return (
    <Layout>
      <DashboardNavbar active={active} setActive={setActive} />
      <DashboardTab active={getActive('balance')}>
        <Balance token={token} idContact={idContact} />
      </DashboardTab>
      <DashboardTab active={getActive('history')}>
        <History token={token} idContact={idContact} />
      </DashboardTab>
      <DashboardTab active={getActive('settings')}>
        <Profile token={token} idContact={idContact} />
      </DashboardTab>
    </Layout>
  )
}

export default DashboardPage
