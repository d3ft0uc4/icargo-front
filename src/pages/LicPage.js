import React, { useState } from 'react'
import Layout from '../components/Layout'
import DashboardTab from '../components/DashboardTab'

const LicPage = () => {
  window.scrollTo(0, 0)
  const [active, setActive] = useState('agreement')
  return (
    <Layout>
      <div className="navbar navbar-expand-lg background-header dashboard-header-wrapper">
        <div className="container" style={{ padding: 0 }}>
          <p style={{ fontSize: '21px', marginBottom: 0 }}>Информация</p>
          <button
            className="navbar-toggle navbar-toggler  navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navbarResponsive2"
            aria-controls="navbarResponsive2"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon icon-bar" />
          </button>
          <div className="collapse navbar-collapse" id="navbarResponsive2">
            <ul className="navbar-nav ml-auto dashboard-header">
              <li className="nav-item dashboard-item">
                <a
                  href="#"
                  className={
                    'nav-link ' + (active === 'agreement' ? 'orange' : '')
                  }
                  onClick={() => setActive('agreement')}
                >
                  Пользовательское соглашение
                </a>
              </li>
              <li className="nav-item dashboard-item">
                <a
                  href="#"
                  className={'nav-link ' + (active === 'offer' ? 'orange' : '')}
                  onClick={() => setActive('offer')}
                >
                  Оферта
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <DashboardTab active={active === 'agreement'}>
      <div className="row">
        <div className="col-lg-12 mt-1 mb-1">
          <h5
              className="orange"
              style={{ float: 'left', paddingTop: '11px', fontWeight: 100 }}
          >
            Пользовательское соглашение
          </h5>
        </div>
      </div>
        <div style={{ fontSize: '16px' }}>
          <div className="row">
            <div className="col-lg-12">
              {' '}
              г. Москва
              <span style={{ float: 'right' }}>
                ред. от{' '}
                <s className="orange" style={{ textDecoration: 'underline' }}>
                  06.02.2020г.
                </s>
              </span>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Текст пользовательского соглашения.
            </div>
          </div>

        </div>
      </DashboardTab>
      <DashboardTab active={active === 'offer'}>
        <div className="row">
          <div className="col-lg-12 mt-1 mb-1">
            <h5
              className="orange"
              style={{ float: 'left', paddingTop: '11px', fontWeight: 100 }}
            >
              Агентский договор-оферта
            </h5>
          </div>
        </div>
        <div style={{ fontSize: '16px' }}>
          <div className="row">
            <div className="col-lg-12">
              {' '}
              г. Москва
              <span style={{ float: 'right' }}>
                ред. от{' '}
                <s className="orange" style={{ textDecoration: 'underline' }}>
                  06.02.2020г.
                </s>
              </span>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Настоящая оферта на заключение агентского договора (далее –
              Оферта, Договор) является публичным предложением ООО
              «КаргоБукинг», именуемого в дальнейшем Принципал, юридическому
              лицу, индивидуальному предпринимателю или физическому лицу,
              именуемому в дальнейшем «Агент», заключить договор на условиях и в
              порядке, определенных настоящей Офертой.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              В соответствии с п. 2 ст. 437 Гражданского Кодекса Российской
              Федерации в случае принятия изложенных ниже условий лицо,
              производящее акцепт Оферты, становится Агентом (в соответствии с
              п. 3 ст. 438 ГК РФ акцепт оферты равносилен заключению договора на
              условиях, изложенных в оферте), а Принципал и Агент совместно —
              Сторонами Договора (далее совместно «Стороны» и отдельно
              «Сторона»).
            </div>
          </div>
          <div className="row mt-5">
            <div className="col-lg-12">
              <h5 style={{ fontSize: '18px', fontWeight: 100 }}>
                Термины и определения
              </h5>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Агент – юридическое лицо, индивидуальный предприниматель или
              физическое лицо, не имеющее статуса индивидуального
              предпринимателя, заключившее с Принципалом настоящий Договор.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Сайт Принципала – интернет-ресурс, расположенный в сети Интернет
              по адресу{' '}
              <a
                href="http://www.cargobooking.ru"
                className="orange hover"
                target="_blank"
              >
                www.cargobooking.ru
              </a>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Услуги Принципала – услуги по организации перевозки груза
              авиационным транспортом, оказываемые Принципалом Клиентам.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Договор перевозки - возмездный договор между Клиентом и
              Принципалом на организацию перевозки груза авиационным
              транспортом.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Привлеченный клиент - юридическое лицо, индивидуальный
              предприниматель или физическое лицо, привлеченное Агентом в рамках
              настоящего Договора, присоединившееся к договору-оферте ООО
              «КаргоБукинг» о предоставлении услуг перевозки груза авиационным
              транспортом и оплатившее перевозку.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Личный кабинета Агента – ресурс, размещенный на сайте Принципала,
              предназначенный для взаимодействия Агента и Принципала.
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-lg-12">
              Оферта (Договор) - настоящий документ, который отражает
              предложение и намерение ООО «КаргоБукинг» считать заключенным
              договор с лицом, которым будет принято предложение на условиях,
              изложенных ниже.
            </div>
          </div>
        </div>
      </DashboardTab>
    </Layout>
  )
}

export default LicPage
